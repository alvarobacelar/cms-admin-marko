function excluirImagem(id) { // função de exclusão da imagem

    var excluir = confirm("Deseja realmente excluir estas IMAGENS?");

    if (excluir) {
        location.href = "includes/controllers/excluirImagem.php?idExcluir=" + id;
    }
}

function dispProduto(id,status) { // função de exclusão da imagem

    var excluir = confirm("Deseja realmente disponibilizar este produto?");

    if (excluir) {
        location.href = "includes/controllers/dispProduto.php?idPrd=" + id + "&status=" + status;
    }
}

// faz verificacao das senhas
function verificaSenha() {
    var senha = document.cadastrar.inputSenha.value;
    var senha2 = document.cadastrar.inputSenha2.value;
    if (senha != senha2) {
        document.getElementById('erro-senha').innerHTML = "As senhas não correspondem";
        return false;
    } else
    if (senha == senha2) {
        document.getElementById('erro-senha').innerHTML = "";
        return true;
    }

}