<?php

require_once './libs/smarty/config/config.php';
require_once './includes/models/ManipulateData.php';
require_once './includes/funcoes/verifica.php';

if ($estaLogado == "SIM") {

    /*
     * Realizando a verificação de retorno de cadastro no banco de dados
     * caso retone uma session com "erroCategoria" o sistema verifica qual mensagem mostrar para o usuário
     */
    if (isset($_SESSION["erroCategoria"])) {
        $erro = $_SESSION["erroCategoria"];
        if ($erro == "duplicado") {
            $smarty->assign("erroCategoria", "<div class='alert alert-danger' role='alert'>Erro! Categoria já cadastrada</div>");
        } else
        if ($erro == "OK") {
            $smarty->assign("erroCategoria", "<div class='alert alert-success' role='alert'>Categoria cadastrado com sucesso!</div>");
        } else {
            $smarty->assign("erroCategoria", "<div class='alert alert-danger' role='alert'>Erro! Se este erro persistir, procure o administrador do sistema</div>");
        }
    } else {
        $smarty->assign("erroCategoria", "");
    }
    unset($_SESSION["erroCategoria"]);

    /*
     * Setando os parâmetros do Smarty da página Categoria
     */
    $local = "<li><a href='./'>Painel Incial</a></li>
        <li class='active'>Nova Categoria</li>";
    $smarty->assign("local", $local);
    $smarty->assign("titulo", "Gerenciar Categoria - Marko");
    $smarty->assign("conteudo", "paginas/gerenciarCategoria.tpl");
    $smarty->display("layout.tpl");
}