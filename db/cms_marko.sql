-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: cms_marko
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria_produto`
--

DROP TABLE IF EXISTS `categoria_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_produto` (
  `id_categoria_produto` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_nome` varchar(70) NOT NULL,
  `data_cadastro` date DEFAULT NULL,
  `status_categoria` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_categoria_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_produto`
--

LOCK TABLES `categoria_produto` WRITE;
/*!40000 ALTER TABLE `categoria_produto` DISABLE KEYS */;
INSERT INTO `categoria_produto` VALUES (1,'InformÃ¡tica','2016-01-16','A'),(2,'Impressoras','2016-01-16','A'),(3,'AcessÃ³rios','2016-01-16','A'),(4,'EscritÃ³rio','2016-01-16','A'),(5,'Smartphone','2016-01-19','A');
/*!40000 ALTER TABLE `categoria_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo_produto`
--

DROP TABLE IF EXISTS `grupo_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo_produto` (
  `id_grupo_produto` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria_produto` int(11) NOT NULL,
  `nome_grupo` varchar(100) NOT NULL,
  `descricao_grupo` text,
  `img_grupo` varchar(100) DEFAULT NULL,
  `data_cadastro_grupo` date DEFAULT NULL,
  `status_grupo` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_grupo_produto`),
  KEY `fk_grupo_produto_categoria_produto1_idx` (`id_categoria_produto`),
  CONSTRAINT `fk_grupo_produto_categoria_produto1` FOREIGN KEY (`id_categoria_produto`) REFERENCES `categoria_produto` (`id_categoria_produto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo_produto`
--

LOCK TABLES `grupo_produto` WRITE;
/*!40000 ALTER TABLE `grupo_produto` DISABLE KEYS */;
INSERT INTO `grupo_produto` VALUES (1,3,'Notebooks',NULL,NULL,'2016-01-16','A'),(2,1,'Desktops','Teste de descriÃ§Ã£o para desktop 1<br>','images/2317dab6948f1f7a162afd84077adc51.jpg','2016-01-16','A'),(3,1,'Tablets',NULL,NULL,'2016-01-16','A'),(4,5,'Smartphones',NULL,NULL,'2016-01-19','A'),(5,4,'Mesas',NULL,NULL,'2016-01-23','A'),(6,2,'Kyocera',NULL,NULL,'2016-01-23','A'),(7,1,'All-in-one',NULL,NULL,'2016-01-23','A'),(8,2,'HP',NULL,NULL,'2016-01-23','A'),(9,3,'Teclados',NULL,NULL,'2016-01-23','A'),(10,3,'Fone de Ouvido',NULL,NULL,'2016-01-23','A'),(11,3,'Mouse','Toda a linha de mouse vocÃª encontra aqui. <br>',NULL,'2016-01-24','A'),(12,4,'Cadeiras','Aqui vocÃª encontra as melhoras cadeiras para seu escritÃ³rio<br>','images/c3c23d3158276a50ccc88a9c462e432b.jpg','2016-01-24','A'),(13,4,'Escrivania','<br>','images/b6310f4d710f071d74dc872689ecdbcb.jpg','2016-01-25','A'),(14,2,'Epson','Descricao<br>','images/dac933e669a3c0cafd6510a3ae412cfa.jpg','2016-02-01','A');
/*!40000 ALTER TABLE `grupo_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagem_produto`
--

DROP TABLE IF EXISTS `imagem_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagem_produto` (
  `id_imagem_produto` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `caminho_imagem` varchar(200) NOT NULL,
  `data_cadastro_imagem` date DEFAULT NULL,
  `imagem_destaque` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_imagem_produto`),
  KEY `fk_imagem_produto_produto1_idx` (`id_produto`),
  CONSTRAINT `fk_imagem_produto_produto1` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id_produto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagem_produto`
--

LOCK TABLES `imagem_produto` WRITE;
/*!40000 ALTER TABLE `imagem_produto` DISABLE KEYS */;
INSERT INTO `imagem_produto` VALUES (114,6,'upload/6/compu23.jpg','2016-01-24',NULL),(115,6,'upload/6/computador1.jpg','2016-01-24',NULL),(118,5,'upload/5/fone3.jpg','2016-01-24',NULL),(119,5,'upload/5/foneouvido.jpg','2016-01-24',NULL),(122,4,'upload/4/1076_3_20130423114953.jpg','2016-01-24',NULL),(123,4,'upload/4/FS1135MFP_zoom.jpg','2016-01-24',NULL),(124,3,'upload/3/Dell.jpg','2016-01-24',NULL),(125,3,'upload/3/dellinspiron14.jpg','2016-01-24',NULL),(126,1,'upload/1/bigphone1.jpg','2016-01-24',NULL),(127,1,'upload/1/bigphone2.jpg','2016-01-24',NULL),(128,1,'upload/1/bigphone3.jpg','2016-01-24',NULL),(129,1,'upload/1/bigphone4.jpg','2016-01-24',NULL),(133,2,'upload/2/samsunggalaxynote4.jpg','2016-01-24',NULL),(134,2,'upload/2/samsunggalaxys6active.jpg','2016-01-24',NULL),(135,2,'upload/2/celNovo.jpg','2016-01-24',NULL),(136,7,'upload/7/cadeira1.jpg','2016-01-25',NULL),(137,7,'upload/7/cadeira2.jpg','2016-01-25',NULL),(138,7,'upload/7/cadeira3.jpg','2016-01-25',NULL),(139,8,'upload/8/escrivania1.jpg','2016-01-25',NULL),(140,8,'upload/8/escrivania2.jpg','2016-01-25',NULL),(141,9,'upload/9/escrinavia20.jpg','2016-01-27',NULL),(142,9,'upload/9/escrivania10.jpg','2016-01-27',NULL),(143,10,'upload/10/cadeira10.jpg','2016-01-27',NULL),(144,10,'upload/10/cadeira20.jpg','2016-01-27',NULL),(145,10,'upload/10/cadeira30.jpg','2016-01-27',NULL),(146,11,'upload/11/notebookacerintelcorei34gb500gbe547130aq14ledwindows81.jpg','2016-02-01',NULL),(147,11,'upload/11/notebookacerintelcorei34gb500gbe547130aq14ledwindows812.jpg','2016-02-01',NULL);
/*!40000 ALTER TABLE `imagem_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_usuario`
--

DROP TABLE IF EXISTS `log_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_usuario` (
  `id_log_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `data_acesso` date DEFAULT NULL,
  `obs_acesso` text,
  PRIMARY KEY (`id_log_usuario`),
  KEY `fk_log_usuario_usuario1_idx` (`id_usuario`),
  CONSTRAINT `fk_log_usuario_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_usuario`
--

LOCK TABLES `log_usuario` WRITE;
/*!40000 ALTER TABLE `log_usuario` DISABLE KEYS */;
INSERT INTO `log_usuario` VALUES (1,0,'2016-01-14','Tentativa de acesso com o usuÃ¡rio: <strong></strong> e Senha: <strong></strong>'),(2,0,'2016-01-14','Tentativa de acesso com o usuÃ¡rio: <strong></strong> e Senha: <strong></strong>'),(3,0,'2016-01-14','Tentativa de acesso com o usuÃ¡rio: <strong></strong> e Senha: <strong></strong>'),(4,0,'2016-01-14','UsuÃ¡rio registrado'),(5,0,'2016-01-14','UsuÃ¡rio registrado'),(6,0,'2016-01-14','UsuÃ¡rio registrado'),(7,0,'2016-01-15','Tentativa de acesso com o usuÃ¡rio: <strong>alvaro</strong> e Senha: <strong>258276</strong>'),(8,0,'2016-01-15','UsuÃ¡rio registrado'),(9,0,'2016-01-15','UsuÃ¡rio registrado'),(10,0,'2016-01-15','UsuÃ¡rio registrado'),(11,0,'2016-01-15','UsuÃ¡rio registrado'),(12,0,'2016-01-15','UsuÃ¡rio registrado'),(13,0,'2016-01-16','UsuÃ¡rio registrado'),(14,0,'2016-01-16','UsuÃ¡rio registrado'),(15,0,'2016-01-16','UsuÃ¡rio registrado'),(16,0,'2016-01-17','UsuÃ¡rio registrado'),(17,0,'2016-01-18','UsuÃ¡rio registrado'),(18,0,'2016-01-18','UsuÃ¡rio registrado'),(19,0,'2016-01-19','UsuÃ¡rio registrado'),(20,0,'2016-01-19','UsuÃ¡rio registrado'),(21,0,'2016-01-19','UsuÃ¡rio registrado'),(22,0,'2016-01-19','UsuÃ¡rio registrado'),(23,0,'2016-01-19','UsuÃ¡rio registrado'),(24,0,'2016-01-20','UsuÃ¡rio registrado'),(25,0,'2016-01-20','UsuÃ¡rio registrado'),(26,0,'2016-01-20','UsuÃ¡rio registrado'),(27,0,'2016-01-21','UsuÃ¡rio registrado'),(28,0,'2016-01-21','UsuÃ¡rio registrado'),(29,0,'2016-01-22','UsuÃ¡rio registrado'),(30,0,'2016-01-22','UsuÃ¡rio registrado'),(31,0,'2016-01-22','UsuÃ¡rio registrado'),(32,2,'2016-01-22','UsuÃ¡rio registrado'),(33,2,'2016-01-22','UsuÃ¡rio registrado'),(34,2,'2016-01-23','UsuÃ¡rio registrado'),(35,2,'2016-01-23','UsuÃ¡rio registrado'),(36,2,'2016-01-23','UsuÃ¡rio registrado'),(37,2,'2016-01-23','UsuÃ¡rio registrado'),(38,2,'2016-01-24','UsuÃ¡rio registrado'),(39,2,'2016-01-24','UsuÃ¡rio registrado'),(40,2,'2016-01-24','UsuÃ¡rio registrado'),(41,2,'2016-01-25','UsuÃ¡rio registrado'),(42,2,'2016-01-25','UsuÃ¡rio registrado'),(43,2,'2016-01-26','UsuÃ¡rio registrado'),(44,2,'2016-01-27','UsuÃ¡rio registrado'),(45,2,'2016-01-27','UsuÃ¡rio registrado'),(46,2,'2016-01-28','UsuÃ¡rio registrado'),(47,2,'2016-01-28','UsuÃ¡rio registrado'),(48,2,'2016-01-28','UsuÃ¡rio registrado'),(49,2,'2016-01-28','UsuÃ¡rio registrado'),(50,2,'2016-02-01','UsuÃ¡rio registrado');
/*!40000 ALTER TABLE `log_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca_produto`
--

DROP TABLE IF EXISTS `marca_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca_produto` (
  `id_marca_produto` int(11) NOT NULL AUTO_INCREMENT,
  `nome_marca` varchar(100) NOT NULL,
  `descricao_marca` text,
  `data_cadastro_marca` date DEFAULT NULL,
  `status_marca` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_marca_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca_produto`
--

LOCK TABLES `marca_produto` WRITE;
/*!40000 ALTER TABLE `marca_produto` DISABLE KEYS */;
INSERT INTO `marca_produto` VALUES (1,'Dell','Marca para notebooks<br>','2016-01-16','A'),(2,'Acer','<br>','2016-01-16','A'),(3,'Samsung','<br>','2016-01-19','A'),(4,'Kyocera','<br>','2016-01-23','A'),(5,'Positivo','<br>','2016-01-23','A'),(6,'Fenix','<br>','2016-01-25','A');
/*!40000 ALTER TABLE `marca_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id_produto` int(11) NOT NULL AUTO_INCREMENT,
  `id_marca_produto` int(11) NOT NULL,
  `id_grupo_produto` int(11) NOT NULL,
  `nome_produto` varchar(200) CHARACTER SET utf8 NOT NULL,
  `cod_produto` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `preco_produto` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `preco_desconto_produto` varchar(45) NOT NULL,
  `porcent_desconto_produto` varchar(10) DEFAULT NULL,
  `descricao_produto` text CHARACTER SET utf8,
  `visao_geral_produto` text CHARACTER SET utf8,
  `info_adicional_produto` text CHARACTER SET utf8,
  `link_video_produto` varchar(150) CHARACTER SET utf8 DEFAULT NULL,
  `disponibilidade_produto` varchar(1) DEFAULT NULL,
  `destaque_produto` varchar(1) DEFAULT NULL,
  `especial_produto` varchar(1) DEFAULT NULL,
  `mais_vendido_produto` varchar(1) DEFAULT NULL,
  `novo_produto` varchar(1) DEFAULT NULL,
  `data_cadastro_produto` date DEFAULT NULL,
  `status_produto` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_produto`),
  KEY `fk_produto_marca_produto1_idx` (`id_marca_produto`),
  KEY `fk_produto_grupo_produto1_idx` (`id_grupo_produto`),
  CONSTRAINT `fk_produto_grupo_produto1` FOREIGN KEY (`id_grupo_produto`) REFERENCES `grupo_produto` (`id_grupo_produto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_produto_marca_produto1` FOREIGN KEY (`id_marca_produto`) REFERENCES `marca_produto` (`id_marca_produto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=big5;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (1,3,4,'Samsun Galaxy Ace','483512569','760,70','1.000,00',NULL,'<p>The perfect mix of portability and performance in a slim 1\" form factor:</p>\r\n        										<ul class=\"product-details-list\"><li>3rd gen IntelÂ® Coreâ„¢ i7 quad core processor available;</li><li>Windows 8 Pro available;</li><li>13.3\" and 15.5\" screen sizes available;</li><li>Double your battery life with available sheet battery;</li><li>4th gen IntelÂ® Coreâ„¢ i7 processor available;</li><li>Full HD TRILUMINOS IPS touchscreen (1920 x 1080);</li><li>Super fast 512GB PCIe SSD available;</li><li>Ultra-light at just 2.34 lbs.</li><li>And more.</li></ul>','<p>The perfect mix of portability and performance in a slim 1\" form factor:</p>\r\n        										<ul class=\"product-details-list\"><li>3rd gen IntelÂ® Coreâ„¢ i7 quad core processor available;</li><li>Windows 8 Pro available;</li><li>13.3\" and 15.5\" screen sizes available;</li><li>Double your battery life with available sheet battery;</li><li>4th gen IntelÂ® Coreâ„¢ i7 processor available;</li><li>Full HD TRILUMINOS IPS touchscreen (1920 x 1080);</li><li>Super fast 512GB PCIe SSD available;</li><li>Ultra-light at just 2.34 lbs.</li><li>And more.</li></ul>','<div class=\"tab-pane active\" id=\"additional\">\r\n        										<strong>Additional Informations</strong>\r\n        										<p>Quae eum placeat reiciendis enim at dolorem eligendi?</p>\r\n        										<hr>\r\n        										<ul class=\"product-details-list\"><li>Lorem ipsum dolor sit quam</li><li>Consectetur adipisicing elit</li><li>Illum autem tempora officia</li><li>Amet  id odio architecto explicabo </li><li>Voluptatem  laborum veritatis</li><li>Quae laudantium iste libero</li></ul>\r\n        									</div>','','S','S','N','N','','2016-01-19','A'),(2,3,4,'Samsun Galaxy C','483512562','709,00','900,00','','<div class=\"tab-pane active\" id=\"description\">\r\n												<p>The perfect mix of portability and performance in a slim 1\" form factor:</p>\r\n        										<ul class=\"product-details-list\"><li>3rd gen IntelÂ® Coreâ„¢ i7 quad core processor available;</li><li>Windows 8 Pro available;</li><li>13.3\" and 15.5\" screen sizes available;</li><li>Double your battery life with available sheet battery;</li><li>4th gen IntelÂ® Coreâ„¢ i7 processor available;</li><li>Full HD TRILUMINOS IPS touchscreen (1920 x 1080);</li><li>Super fast 512GB PCIe SSD available;</li><li>Ultra-light at just 2.34 lbs.</li><li>And more...</li></ul>\r\n        									</div>','<div class=\"tab-pane active\" id=\"description\">\r\n												<p>The perfect mix of portability and performance in a slim 1\" form factor:</p>\r\n        										<ul class=\"product-details-list\"><li>3rd gen IntelÂ® Coreâ„¢ i7 quad core processor available;</li><li>Windows 8 Pro available;</li><li>13.3\" and 15.5\" screen sizes available;</li><li>Double your battery life with available sheet battery;</li><li>4th gen IntelÂ® Coreâ„¢ i7 processor available;</li><li>Full HD TRILUMINOS IPS touchscreen (1920 x 1080);</li><li>Super fast 512GB PCIe SSD available;</li><li>Ultra-light at just 2.34 lbs.</li><li>And more...</li></ul>\r\n        									</div>','<div class=\"tab-pane active\" id=\"additional\">\r\n        										<strong>Additional Informations</strong>\r\n        										<p>Quae eum placeat reiciendis enim at dolorem eligendi?</p>\r\n        										<hr>\r\n        										<ul class=\"product-details-list\"><li>Lorem ipsum dolor sit quam</li><li>Consectetur adipisicing elit</li><li>Illum autem tempora officia</li><li>Amet  id odio architecto explicabo </li><li>Voluptatem  laborum veritatis</li><li>Quae laudantium iste libero</li></ul>\r\n        									</div>','https://www.youtube.com/embed/KUC4wgBnriI','S','N','N','S','','2016-01-20','A'),(3,1,1,'Notebook Inspiron 14 SÃ©rie 3000','483512510','2.869,00','3.000,00','','Notebook de 14\" que cabe no seu bolso. Cheio de recursos, Ã© ideal para vocÃª que estÃ¡ sempre em movimento.','Notebook de 14\" que cabe no seu bolso. Cheio de recursos, Ã© ideal para vocÃª que estÃ¡ sempre em movimento.','<strong>Maior nitidez com alta definiÃ§Ã£o:</strong> o monitor de 14\" \r\nganha vida com a brilhante resoluÃ§Ã£o de alta definiÃ§Ã£o.Role, digite e \r\ndeslize o dedo em seu Inspiron 14 com touchscreen (disponÃ­vel em algumas\r\n opÃ§Ãµes de oferta) de alta definiÃ§Ã£o que torna a interaÃ§Ã£o com seus \r\naplicativos divertida e fÃ¡cil.<br><br><strong>Performance mais rÃ¡pida: </strong>Com\r\n os processadores mais recentes,oferece muita potÃªncia para vocÃª navegar\r\n na Web e assistir a vÃ­deos. As opÃ§Ãµes de ofertas com placa grÃ¡fica \r\ndedicadade atÃ© 2 GB permitem a realizaÃ§Ã£o das tarefas mais intensas sem \r\nsacrificar a performance.<br><strong><br>Unidade de DVD integrada:</strong>\r\n grave a lista de reproduÃ§Ã£o de sua preferÃªncia, assista a filmes e \r\ncarregue software com uma unidade Ã³ptica integrada. Ou vocÃª pode \r\ntransferir grandes volumes de dados do seu disco rÃ­gido para DVDs para \r\nfÃ¡cil arquivamento.','<script src=','S','S','N','N','S','2016-01-22','A'),(4,4,6,'Impressora Multifuncional Kyocera FS 1120 MFP Laser Mono','483512512','1.500,90','2.000,00','-30%','<h3 itemprop=\"description\" class=\"descricao-full\"><p><font size=\"3\">VersÃ¡til e \r\ncompacta, a Multifuncional Kyocera FS-1120MFP Ã© uma excelente opÃ§Ã£o para\r\n escritÃ³rios e home offices, funcionando como impressora, scanner e fax.\r\n Com memÃ³ria de 64 MB, ela trabalha muito bem em rede, alÃ©m de ter um \r\nbaixo nÃ­vel de ruÃ­do quando em funcionamento.</font></p>\r\n\r\n<p><font size=\"3\">A FS-1120MFP possui bandeja de 250 folhas para impressÃ£o, e \r\nalimentador automÃ¡tico de 40 folhas para o scanner, otimizando o \r\ntrabalho e eliminando a necessidade de constantes intervenÃ§Ãµes do \r\nusuÃ¡rio para alimentar o equipamento. A impressora leva menos de 8,5 \r\nsegundos para imprimir o primeiro documento, e a resoluÃ§Ã£o mÃ¡xima Ã© de \r\n1800x600 dpi, resultando em impressÃµes de qualidade profissional.</font></p>\r\n\r\n<p><font size=\"3\">O scanner da Kyocera FS-1120MFP possui resoluÃ§Ã£o mÃ¡xima de 600dpi e \r\nconectividade com USB 2.0 de alta velocidade. A velocidade de \r\ntransmissÃ£o do fax Ã© de menos de 4 segundos por pÃ¡gina.</font></p></h3>','<h3 itemprop=\"description\" class=\"descricao-full\"><p><font size=\"3\">VersÃ¡til e \r\ncompacta, a Multifuncional Kyocera FS-1120MFP Ã© uma excelente opÃ§Ã£o para\r\n escritÃ³rios e home offices, funcionando como impressora, scanner e fax.\r\n Com memÃ³ria de 64 MB, ela trabalha muito bem em rede, alÃ©m de ter um \r\nbaixo nÃ­vel de ruÃ­do quando em funcionamento.</font></p>\r\n\r\n<p><font size=\"3\">A FS-1120MFP possui bandeja de 250 folhas para impressÃ£o, e \r\nalimentador automÃ¡tico de 40 folhas para o scanner, otimizando o \r\ntrabalho e eliminando a necessidade de constantes intervenÃ§Ãµes do \r\nusuÃ¡rio para alimentar o equipamento. A impressora leva menos de 8,5 \r\nsegundos para imprimir o primeiro documento, e a resoluÃ§Ã£o mÃ¡xima Ã© de \r\n1800x600 dpi, resultando em impressÃµes de qualidade profissional.</font></p>\r\n\r\n<p><font size=\"3\">O scanner da Kyocera FS-1120MFP possui resoluÃ§Ã£o mÃ¡xima de 600dpi e \r\nconectividade com USB 2.0 de alta velocidade. A velocidade de \r\ntransmissÃ£o do fax Ã© de menos de 4 segundos por pÃ¡gina.</font></p></h3>','asamsakmcksmklamckamskmak<br>','https://www.youtube.com/embed/KUC4wgBnriI','S','N','S','N','S','2016-01-23','A'),(5,3,10,'FONE DE OUVIDO C/MIC. LIFECHAT LX-3000 JUG-00013 MICROSOFT','306027','169,00','','','Som estÃ©reo de alta qualidade<br>Ãudio de alta qualidade para falar ao telefone ou ouvir mÃºsica.<br>Som superior com conectividade digital USB 2.0<br>Excelentes confiabilidade e clareza.<br>Microfone anulador de ruÃ­dos<br>Capta automaticamente a sua voz com uma nitidez notÃ¡vel.<br>Fones de ouvido confortÃ¡veis feitos de couro sintÃ©tico<br>OuÃ§a e reproduza por horas em conforto total.<br>OtimizaÃ§Ã£o para o Skype<br>FaÃ§a mais junto, com os produtos da Microsoft certificados para o Skype.<br>Ideal para bate-papo com voz e vÃ­deo<br>OuÃ§a amigos e familiares como se eles estivessem bem perto.<br>Ideal para ouvir mÃºsica<br>OuÃ§a suas bandas preferidas em Ã¡udio de alta qualidade.<br><br>','Som estÃ©reo de alta qualidade<br>Ãudio de alta qualidade para falar ao telefone ou ouvir mÃºsica.<br>Som superior com conectividade digital USB 2.0<br>Excelentes confiabilidade e clareza.<br>Microfone anulador de ruÃ­dos<br>Capta automaticamente a sua voz com uma nitidez notÃ¡vel.<br>Fones de ouvido confortÃ¡veis feitos de couro sintÃ©tico<br>OuÃ§a e reproduza por horas em conforto total.<br>OtimizaÃ§Ã£o para o Skype<br>FaÃ§a mais junto, com os produtos da Microsoft certificados para o Skype.<br>Ideal para bate-papo com voz e vÃ­deo<br>OuÃ§a amigos e familiares como se eles estivessem bem perto.<br>Ideal para ouvir mÃºsica<br>OuÃ§a suas bandas preferidas em Ã¡udio de alta qualidade.<br><br>','<br>','','S','N','N','N','S','2016-01-23','A'),(6,1,2,'COMPUTADOR ALL IN ONE DELL OPTIPLEX 3030-B10, INTEL CORE I3 3.5GHZ, 4GB RAM, 500GB, TELA 19.5â€™â€™, WINDOWS 8.1','3060272','3.083,00','','','ESPECIFICAÃ‡Ã•ES TÃ‰CNICAS <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tela&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; WLED de 19.5\", 1.600 x 900 e resoluÃ§Ã£o HD+<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Processador&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intel Core i3-4150 3.50GHz (cache 3MB, TDP 54W)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Disco RÃ­gido (HD)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 500GB SATA3 5400 RPM HDD<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MemÃ³ria RAM&nbsp; 4GB DDR3L 1600MHz<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Drive Ã“ptico&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DVD+/-RW<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sistema Operacional&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Windows 8.1 SL<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Placa de VÃ­deo&nbsp; Intel HD Graphics 4400<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Chipset Intel H81 Express<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Placa de Rede&nbsp;&nbsp; Realtek RTL8151GD GIGABIT ETHERNET Lan 10/100/1000 integrada<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CÃ¢mera Embutida&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 720p alta definiÃ§Ã£o<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Teclado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sim<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mouse Sim<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Entradas e SaÃ­das&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 portas USB 3.0 externas (laterais)<br>4 portas USB 2.0 externas (traseiras)<br>1 RJ-45<br>1 porta VGA<br>1 porta universal headset<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ãudio&nbsp;&nbsp;&nbsp; Realtek ALC3661 High Definition Ãudio Codec<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Leitor de CartÃ£o&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SD, SDXC, SDHC, MMC (Multi Media Card) (4 in 1 Card)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tecnologia Wireless&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NÃ£o<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Alto-Falantes&nbsp;&nbsp;&nbsp; Alto-falante da Dell interno (padrÃ£o)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Energia Energy Star 6.0<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fonte de AlimentaÃ§Ã£o&nbsp; Internal PSU 180W<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Voltagem&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bivolt<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Peso&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,1 Kg<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DimensÃµes (LxAxP)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 32,8 x 48.9 x 6,7 cm<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ConteÃºdo da Embalagem&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Computador<br>- Adaptador AC<br>- Cabo de forÃ§a<br>- Manuais<br><br>- Teclado e mouse<br><br>','ESPECIFICAÃ‡Ã•ES TÃ‰CNICAS <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tela&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; WLED de 19.5\", 1.600 x 900 e resoluÃ§Ã£o HD+<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Processador&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intel Core i3-4150 3.50GHz (cache 3MB, TDP 54W)<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Disco RÃ­gido (HD)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 500GB SATA3 5400 RPM HDD<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MemÃ³ria RAM&nbsp; 4GB DDR3L 1600MHz<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Drive Ã“ptico&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DVD+/-RW<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sistema Operacional&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Windows 8.1 SL<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Placa de VÃ­deo&nbsp; Intel HD Graphics 4400<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Chipset Intel H81 Express<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Placa de Rede&nbsp;&nbsp; Realtek RTL8151GD GIGABIT ETHERNET Lan 10/100/1000 integrada<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CÃ¢mera Embutida&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 720p alta definiÃ§Ã£o<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Teclado&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sim<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mouse Sim<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Entradas e SaÃ­das&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 portas USB 3.0 externas (laterais)<br>','<br>','','S','N','N','S','S','2016-01-23','A'),(7,6,12,'Cadeira de EscritÃ³rio Vancouver Preta','483512123',' 272,99 ','499,99 ','-35%','Para manter sua coluna reta de maneira confortÃ¡vel e evitar que vocÃª prejudique sua saÃºde, o encosto da <strong>cadeira</strong> Ã© bastante amplo e possui a curvatura certa para lhe fornecer bem-estar. Ele ainda Ã© muito resistente e duradouro.','<p class=\"title\">Mais praticidade, melhor rendimento.<br>\r\n            </p>\r\n                Devemos nos sentir <strong>Ã  vontade</strong> \r\nenquanto trabalhamos ou estudamos, nÃ£o Ã© verdade? Contar com uma mesa \r\nampla, um lugar adequado para nossos instrumentos e livros e manter o \r\nambiente organizado sÃ£o quesitos fundamentais para <strong>atingir Ã³timos resultados</strong>. TÃ£o importante quanto tudo isso Ã© ter a <strong>cadeira certa</strong> para passar todo o tempo sentado!','Os <strong>rodÃ­zios</strong> sÃ£o uma maravilha para garantir mais <strong>praticidade e comodidade</strong> Ã  sua rotina. Com eles vocÃª pode ir para lÃ¡ e para cÃ¡ sem dificuldades, nÃ£o precisando se levantar. Esta <strong>cadeira de escritÃ³rio</strong> ainda conta com <strong>ajuste de altura</strong> e Ã© <strong>giratÃ³ria</strong>, tudo para suas mais variadas necessidades e gostos, para a seu pleno conforto!','','S','N','N','N','S','2016-01-25','A'),(8,6,13,'Escrivaninha Sesto 4 Gavetas Branco','483512199','209,29','499,99','-25%','Na hora de <strong>trabalhar</strong> ou <strong>estudar</strong>, nada como ter um cantinho aconchegante para deixar vocÃª mais <strong>confortÃ¡vel</strong>, nÃ£o Ã© mesmo? A <strong>Escrivaninha Malta</strong> Ã© ideal para fazer parte do seu quarto, sala ou <strong>escritÃ³rio</strong>. Contando com 4 <strong>gavetas</strong> de corrediÃ§as metÃ¡licas para guardar pequenos objetos, seu <strong>modelo compacto</strong> na cor <strong>branca</strong> Ã© Ã³timo para quem aprecia uma <strong>decoraÃ§Ã£o clean e tradicional</strong>. Demais, nÃ©? :) ','<strong>nformaÃ§Ãµes importantes:</strong>\r\n<p class=\"mb-5\">- Os objetos que ambientam as fotos nÃ£o acompanham o produto.</p>\r\n<p>- Verifique as dimensÃµes do produto e certifique-se que o percurso \r\nque ele farÃ¡ atÃ© o local de uso permite sua passagem. NÃ³s nÃ£o nos \r\nresponsabilizamos por transporte de </p>','                                Necessita Montagem?                            \r\n                            \r\n                                <span class=\"dynamic_option_assembly_required\">Sim,\r\n sugerimos a contrataÃ§Ã£o do nosso serviÃ§o de montagem (para os CEPs em \r\nque o serviÃ§o estÃ¡ disponÃ­vel) ou de algum profissional experiente de \r\nsua preferÃªncia</span>','','S','N','N','N','S','2016-01-25','A'),(9,6,13,' EstaÃ§Ã£o de Trabalho Importado Office Fefs - Cinza','306027212','399,00','','',' Material: AÃ§o e vidro temperado\r\n<br>- Acabamento Interno: AÃ§o\r\n<br>- Acabamento Externo: AÃ§o e vidro temperado\r\n<br>- Cor: Cinza e preta\r\n<br>\r\n<br><strong>EspecificaÃ§Ãµes tÃ©cnicas da estaÃ§Ã£o de trabalho Fefs Importado:</strong>\r\n<br>- Origem do Produto: Importado\r\n<br>- DimensÃµes da Embalagem (LxAxP): 170 x 920 x 520 mm\r\n<br>- Peso da Embalagem (Kg): 47,5 Kg\r\n                                        ','AÃ§o e vidro temperado\r\n                                                                    \r\n                        \r\n                            <dl id=\"ctl00_Conteudo_ctl56_DetalhesProduto_rptGrupos_ctl01_rptCampos_ctl02_dlCategoria\" class=\"Largura--cm- even\"><li>\r\n                                    Altura (cm)\r\n                                </li><li>\r\n                                            95</li><li>Largura (cm)\r\n                                </li><li>\r\n                                            145</li></dl>','<br>','','S','N','N','N','S','2016-01-27','A'),(10,6,12,' Cadeira Office Importado Austin em PU e PVC','12232334','299,00','399,00','-25%','- Cadeira Office GiratÃ³ria\r\n<br>- Revestimento em PU e PVC\r\n<br>- Base de Metal cromado\r\n<br>- Nylon Caster\r\n<br>- RodÃ­zios\r\n','<dl id=\"ctl00_Conteudo_ctl58_DetalhesProduto_rptGrupos_ctl02_rptCampos_ctl00_dlCategoria\" class=\"Entrega-do-Produto\"><dd>Todas as instruÃ§Ãµes, manuais e peÃ§as necessÃ¡rias para a montagem sÃ£o fornecidas junto com o produto,&nbsp;\r\n                                        </dd><dd>\r\n                                            nÃ£o nos responsabilizamos pela instalaÃ§Ã£o/montagem,&nbsp;\r\n                                        </dd><dd>\r\n                                            orientamos que este produto \r\nseja montado por um tÃ©cnico de sua confianÃ§a, apÃ³s a sua entrega,&nbsp;\r\n                                        </dd><dd>\r\n                                            NÃ£o nos responsabilizamos \r\npelo transporte por escadas/elevadores, guincho ou iÃ§amento deste \r\nproduto.\r\n                                        </dd></dl>','<dl><dt>Altura</dt><dd>1,11 Metros</dd><dt>Largura</dt><dd>58,00 Centimetros</dd><dt>Profundidade</dt><dd>67,00 Centimetros</dd></dl>','','S','N','N','N','S','2016-01-27','A'),(11,1,1,'Notebook Acer Intel Core i3 4GB 500GB E5-471-30AQ 14\' LED Windows 8.1','12323212','1.799,00','','',' Acer Aspire E5 possui design moderno, fino e leve. Indicado para \r\nconsumidores que procuram uma mÃ¡quina para executar as tarefas do \r\ncotidiano com velocidade e praticidade, pode ser usado para o trabalho, \r\nestudos ou lazer.','Equipado com processador IntelÂ® Coreâ„¢ i3-4005U, apresenta memÃ³ria RAM de\r\n 4GB e HD com capacidade de 500GB, espaÃ§o suficiente para instalar \r\nprogramas sempre que necessÃ¡rio e armazenar diversos arquivos, como \r\nmÃºsicas e fotos.','<br>','','S','N','N','N','S','2016-02-01','A');
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome_usuario` varchar(100) DEFAULT NULL,
  `login_usuario` varchar(45) NOT NULL,
  `senha_usuario` varchar(100) NOT NULL,
  `nivel_usuario` varchar(10) DEFAULT NULL,
  `status_usuario` varchar(2) DEFAULT NULL,
  `data_cadastro_usuario` date DEFAULT NULL,
  `funcao_usuario` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'OFF','OFF','OFF','OFF','A',NULL,NULL),(2,'Alvaro Bacelar de Sousa','admin','202cb962ac59075b964b07152d234b70','0','A',NULL,NULL),(3,'Alberone','irmao','202cb962ac59075b964b07152d234b70','0','A','2016-01-22',NULL);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-01 13:18:29
