<?php

require_once './libs/smarty/config/config.php';
require_once './includes/models/ManipulateData.php';
require_once './includes/funcoes/verifica.php';

if ($estaLogado == "SIM"){
    
    /*
     * Realizando a verificação de retorno de cadastro no banco de dados
     * caso retone uma session com "erroCategoria" o sistema verifica qual mensagem mostrar para o usuário
     */
    if (isset($_SESSION["erroGrupo"])) {
        $erro = $_SESSION["erroGrupo"];
        if ($erro == "editado") {
            $smarty->assign("erroGrupo", "<div class='alert alert-success' role='alert'>Produto Editado com sucesso!</div>");
        } else
        if ($erro == "OK") {
            $smarty->assign("erroGrupo", "<div class='alert alert-success' role='alert'>Grupo cadastrado com sucesso!</div>");
        } else {
            $smarty->assign("erroGrupo", "<div class='alert alert-danger' role='alert'>Erro! ". $_SESSION["erroGrupo"] . "</div> ");
        }
    } else {
        $smarty->assign("erroGrupo", "");
    }
    unset($_SESSION["erroGrupo"]);
    
    /*
     * Realizando pesquisa no banco de dados de grupos de produtos cadastrados
     */
    $produto = new ManipulateData();
    $produto->setTable("grupo_produto, categoria_produto");
    $produto->setOrderTable("WHERE grupo_produto.id_categoria_produto = categoria_produto.id_categoria_produto ORDER BY nome_grupo");
    $produto->select();
    while ($db[] = $produto->fetch_object()){
        $smarty->assign("produto", $db);
    } // FIM DA PESQUISA DE GRUPOS DE PRODUTOS
    
    $local = "<li><a href='./'>Painel Incial</a></li>
        <li class='active'>Grupos Cadastrados</li>";
    $smarty->assign("local", $local);
    $smarty->assign("titulo", "Grupos Cadastrados - Marko");
    $smarty->assign("conteudo", "paginas/gruposCadastrados.tpl");
    $smarty->display("layout.tpl");
    
}