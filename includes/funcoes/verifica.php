<?php

if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION["idSession"])) {
    if (!isset($_SESSION["erro"])) {
        $_SESSION["erro"] = "erro_sessao";
    }
    $smarty->assign('logado', 'NAO');
    $estaLogado = "NAO";
    $smarty->assign('nivel', 'NI');
    $local = 0;
} else {
    // autentica o usuario
    $nivel = $_SESSION['nivel'];
    $funcao = $_SESSION['funcao'];
    $smarty->assign("nomeUser", $_SESSION["nome"]);
    $smarty->assign("funcao", $funcao);
    $smarty->assign("versao", "1.0");
    $estaLogado = "SIM";
}

if ($estaLogado == "NAO") {
    // verifica se houve erro no login
    if ($_SESSION["erro"] == "erro") {
        $smarty->assign("erro", "<div class='alert alert-danger' role='alert'>Usuario ou senha não correspondem</div>");
    } else {
        $smarty->assign("erro", "&nbsp;");
    }
    unset($_SESSION["erro"]); // destroi a session do erro
    // chama a tela de login caso não houver session estartada
    $smarty->assign("titulo", "Login - Marko Informática");
    $smarty->assign("versao", "1.0");
    $smarty->display("login.tpl");
}
 
