<?php

require_once '../models/ManipulateData.php';

/*
 * CADASTRO DE CATEGORIA
 */

session_start();


$marca = addslashes($_POST["inputMarca"]);
$descricaoMarca = addslashes($_POST["textAreaDescMarca"]);
$dataMarca = date("Y-m-d");

if (!empty($marca)) {
    //CAPTANDO DADOS DO FORMULARIO
    //INSTACIANDO O OBJETO DE CADASTRO
    $cad = new ManipulateData(); //INSTACIANDO A CLASSE
    $cad->setTable("marca_produto"); //SETANDO O NOME DA TABELA
    $cad->setCampoTable("nome_marca");

    //VERIFICANDO SE EXISTE REGISTRO CADASTRADO
    if ($cad->getDadosDuplicados("$marca") >= 1) {
        $_SESSION["erroMarca"] = "duplicado";
        header("location: ../../gerenciarMarca.php");
    } else {
        $cad->setCamposBanco("nome_marca, descricao_marca, data_cadastro_marca, status_marca"); //CAMPOS DO BANCO DE DADOS
        $cad->setDados("'$marca', '$descricaoMarca', '$dataMarca', 'A'"); //DADOS DO FORMULARIOS (O status como A siginifica que está ativo)
        $cad->insert(); //EFETUANDO CADASTRO
        $_SESSION["erroMarca"] = "OK";
        header("location: ../../gerenciarMarca.php");
    }
} else {
    header("Location: ../../erro.php");
}
