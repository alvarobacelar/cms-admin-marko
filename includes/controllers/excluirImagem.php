<?php

session_start();
require_once '../models/ManipulateData.php';

if ($_SESSION["nivel"] == "admin" || $_SESSION["nivel"] == "gerente"){

if (isset($_GET["idExcluir"])) {
    $idProduto = addslashes($_GET["idExcluir"]);
    $pasta = "../../../upload/" . $idProduto ."/";

    $exclui = new ManipulateData();
    $exclui->setTable("imagem_produto");
    $exclui->setCampoTable("id_produto");
    $exclui->setValueId("$idProduto");
    $exclui->delete();

    /*
     * Realizando a exclusão das imagens da pasta do produto selecionado.
     */
    if (is_dir($pasta)) {
        $diretorio = dir($pasta);

        while ($arquivo = $diretorio->read()) {
            if (($arquivo != '.') && ($arquivo != '..')) {
                unlink($pasta . $arquivo);
            }
        }
        $diretorio->close();
    }

    $_SESSION["erroImagem"] = "excluido";
    header("location: ../../produtosCadastrados.php");
} else {
    header("location: ../../erro.php");
}
     
} else {
    header("location: ../../accessDenied.php");
}