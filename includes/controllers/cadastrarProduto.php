<?php

require_once '../models/ManipulateData.php';

/*
 * CADASTRO DE PRODUTO
 */

session_start();

//CAPTANDO DADOS DO FORMULARIO
$secGrupo = addslashes($_POST["selectGrupo"]);
$secMarca = addslashes($_POST["selectMarca"]);
$inputProduto = addslashes($_POST["inputProduto"]);
$inputCod = addslashes($_POST["inputCod"]);
$inputPreco = addslashes($_POST["inputPreco"]);
$precoDesconto = addslashes($_POST["inputPrecoDesconto"]);
$porcentDesconto = addslashes($_POST["inputPorcentDesconto"]);
$textAreaDescricao = addslashes($_POST["textAreaDescricao"]);
$textAreaVisGeral = addslashes($_POST["textAreaVisGeral"]);
$textAreaInfoAdicinal = addslashes($_POST["textAreaInfoAdicinal"]);
$inputLinkVideo = addslashes($_POST["inputLinkVideo"]);
$destaque = addslashes($_POST["chekDestaque"]);
$especiais = addslashes($_POST["chekEspeciais"]);
$novo = addslashes($_POST["chekNovo"]);
$vendidos = addslashes($_POST["chekMaisVendidos"]);
$dataGrupo = date("Y-m-d");

if (!empty($especiais)){
    $especialBD = "S";
} else  {
    $especialBD = "N";
}
if (!empty($vendidos)){
    $vendidoBD = "S";
} else  {
    $vendidoBD = "N";
}
if (!empty($destaque)){
    $destaqueBD = "S";
} else  {
    $destaqueBD = "N";
}
if (!empty($novo)){
    $novoBD = "S";
} else  {
    $novoBD = "";
}

if (!empty($inputProduto) && !empty($inputCod)) {
    
    //INSTACIANDO O OBJETO DE CADASTRO
    $cad = new ManipulateData(); //INSTACIANDO A CLASSE
    $cad->setTable("produto"); //SETANDO O NOME DA TABELA
    $cad->setCampoTable("cod_produto"); // SETANDO O NOME DO CAMPO DO BANCO QUE VAI VERIFICAR DUPLICIDADE

    //VERIFICANDO SE EXISTE REGISTRO CADASTRADO
    if ($cad->getDadosDuplicados("$inputCod") >= 1) {
        $_SESSION["erroProduto"] = "duplicado";
        header("location: ../../gerenciarProduto.php");
    } else {
        $cad->setCamposBanco("id_marca_produto,id_grupo_produto,nome_produto,cod_produto,preco_produto,preco_desconto_produto,porcent_desconto_produto,descricao_produto,visao_geral_produto,info_adicional_produto,link_video_produto, disponibilidade_produto, destaque_produto, especial_produto, mais_vendido_produto, novo_produto, data_cadastro_produto,status_produto"); //CAMPOS DO BANCO DE DADOS
        $cad->setDados("'$secMarca', '$secGrupo', '$inputProduto','$inputCod','$inputPreco','$precoDesconto','$porcentDesconto','$textAreaDescricao','$textAreaVisGeral','$textAreaInfoAdicinal', '$inputLinkVideo','N', '$destaqueBD', '$especialBD','$vendidoBD','$novoBD', '$dataGrupo', 'A'"); //DADOS DO FORMULARIOS
        $cad->insert(); //EFETUANDO CADASTRO
        $_SESSION["erroProduto"] = "OK";
        $_SESSION["idProdutoNovo"] = mysql_insert_id(); 
        header("location: ../../gerenciarProduto.php");
    }
} else {
    header("Location: ../../erro.php");
}
