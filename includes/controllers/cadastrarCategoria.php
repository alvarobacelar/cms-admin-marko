<?php

require_once '../models/ManipulateData.php';

/*
 * CADASTRO DE CATEGORIA
 */

session_start();


$categoria = addslashes($_POST["inputCategoria"]);
$dataCategoria = date("Y-m-d");

if (!empty($categoria)) {
    //CAPTANDO DADOS DO FORMULARIO
    //INSTACIANDO O OBJETO DE CADASTRO
    $cad = new ManipulateData(); //INSTACIANDO A CLASSE
    $cad->setTable("categoria_produto"); //SETANDO O NOME DA TABELA
    $cad->setCampoTable("categoria_nome");

    //VERIFICANDO SE EXISTE REGISTRO CADASTRADO
    if ($cad->getDadosDuplicados("$categoria") >= 1) {
        $_SESSION["erroCategoria"] = "duplicado";
        header("location: ../../gerenciarCategoria.php");
    } else {
        $cad->setCamposBanco("categoria_nome,data_cadastro,status_categoria"); //CAMPOS DO BANCO DE DADOS
        $cad->setDados("'$categoria', '$dataCategoria', 'A'"); //DADOS DO FORMULARIOS
        $cad->insert(); //EFETUANDO CADASTRO
        $_SESSION["erroCategoria"] = "OK";
        header("location: ../../gerenciarCategoria.php");
    }
} else {
    header("Location: ../../erro.php");
}
