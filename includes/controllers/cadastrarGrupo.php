<?php

require_once '../models/ManipulateData.php';

/*
 * CADASTRO DE CATEGORIA
 */

session_start();


$grupo = addslashes($_POST["inputGrupo"]);
$idCategoria = addslashes($_POST["selectCategoria"]);
$descricaoGp = addslashes($_POST["textAreaDescricaoGp"]);
$dataGrupo = date("Y-m-d");

########################################################################
################ FAZENDO O UPLOAD DE ARQUIVO DA FOTO ###################
########################################################################
//Filedata é a variável que o flex envia com o arquivo para upload
$fotoOM = $_FILES['fileGrupo'];

if (!empty($fotoOM)) {
// Pasta onde o arquivo vai ser salvo
    $_UP['pasta'] = '../../../images/';

// Tamanho máximo do arquivo (em Bytes)
    $_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
// Array com as extensões permitidas
    $_UP['extensoes'] = array('jpg', 'JPG', 'JPEG','jpeg','png','PNG');

// Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
    $_UP['renomeia'] = true;

// Array com os tipos de erros de upload do PHP
    $_UP['erros'][0] = 'Não houve erro';
    $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
    $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
    $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
    $_UP['erros'][4] = 'Não foi feito o upload do arquivo';

// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
    if ($_FILES['fileGrupo']['error'] != 0) {
//                die("Não foi possível fazer o upload, erro:<br />" .
//                        $_UP['erros'][$_FILES['fileFotoMotorista']['error']]);
//                exit; // Para a execução do script
        $_SESSION["erroGrupo"] = "Arquivo inválido";
        header("location: ../../gerenciarGrupo.php");
        exit();
    }

// Caso script chegue a este ponto, não houve erro com o processo de  upload e o PHP pode continuar
// Faz a verificação da extensão do arquivo
// $extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
    $arquivo = $_FILES['fileGrupo']['name'];

    $extensao = substr($arquivo, - 3);

    if (array_search($extensao, $_UP['extensoes']) === false && array_search($extensao2, $_UP['extensoes']) === false) {
//echo "Por favor, envie arquivos com as seguintes extensões: jpg ou JPEG";
        $_SESSION["erroGrupo"] = "Extensao de arquivo inválida";
        header("location: ../../gerenciarGrupo.php");
    }

// Faz a verificação do tamanho do arquivo enviado
    else if ($_UP['tamanho'] < $_FILES['fileGrupo'] ['size']) {
//echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
        $_SESSION["erroGrupo"] = "Tamanho maior que o permitido (2MB)";
        header("location: ../../gerenciarGrupos.php");
    }

// O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
    else {
// Primeiro verifica se deve trocar o nome do arquivo
// Cria um nome baseado no UNIX TIMESTAMP atual e comextensão .jpg
        $nome_final = md5(date("dmyhi") . "grupo") ."."."$extensao";

// Depois verifica se é possível mover o arquivo para a pasta escolhida
        if (move_uploaded_file($_FILES['fileGrupo'] ['tmp_name'], $_UP['pasta'] . $nome_final)) {
// Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
//echo '<br /><a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>;
            $fotoOM = "images/" . $nome_final;
        } else {
            $_SESSION["erroGrupo"] = "Erro ao mover o arquivo para a pasta configurada";
            header("location: ../../gerenciarGrupo.php");
        }
    }
########################################################################
######################## FIM DA FUNÇÃO DE UPLOAD #######################
########################################################################
}

if (!empty($grupo)) {
    //CAPTANDO DADOS DO FORMULARIO
    //INSTACIANDO O OBJETO DE CADASTRO
    $cad = new ManipulateData(); //INSTACIANDO A CLASSE
    $cad->setTable("grupo_produto"); //SETANDO O NOME DA TABELA
    $cad->setCampoTable("nome_grupo");

    //VERIFICANDO SE EXISTE REGISTRO CADASTRADO
    if ($cad->getDadosDuplicados("$grupo") >= 1) {
        $_SESSION["erroGrupo"] = "duplicado";
        header("location: ../../gerenciarGrupo.php");
    } else {
        $cad->setCamposBanco("id_categoria_produto,nome_grupo,descricao_grupo,img_grupo,data_cadastro_grupo,status_grupo"); //CAMPOS DO BANCO DE DADOS
        $cad->setDados("'$idCategoria', '$grupo','$descricaoGp','$fotoOM','$dataGrupo', 'A'"); //DADOS DO FORMULARIOS
        $cad->insert(); //EFETUANDO CADASTRO
        $_SESSION["erroGrupo"] = "OK";
        header("location: ../../gerenciarGrupo.php");
    }
} else {
    header("Location: ../../erro.php");
}
    