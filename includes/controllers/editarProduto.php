<?php

require_once '../models/ManipulateData.php';
session_start();

//CAPTANDO DADOS DO FORMULARIO
$secGrupo = addslashes($_POST["selectGrupo"]);
$secMarca = addslashes($_POST["selectMarca"]);
$inputProduto = addslashes($_POST["inputProduto"]);
$inputCod = addslashes($_POST["inputCod"]);
$inputPreco = addslashes($_POST["inputPreco"]);
$precoDesconto = addslashes($_POST["inputPrecoDesconto"]);
$porcentDesconto = addslashes($_POST["inputPorcentDesconto"]);
$textAreaDescricao = addslashes($_POST["textAreaDescricao"]);
$textAreaVisGeral = addslashes($_POST["textAreaVisGeral"]);
$textAreaInfoAdicinal = addslashes($_POST["textAreaInfoAdicinal"]);
$inputLinkVideo = addslashes($_POST["inputLinkVideo"]);
$destaque = addslashes($_POST["chekDestaque"]);
$especiais = addslashes($_POST["chekEspeciais"]);
$vendidos = addslashes($_POST["chekMaisVendidos"]);
$novo = addslashes($_POST["chekNovo"]);
$idProduto = addslashes($_POST["inputIdProduto"]);

if (!empty($especiais)){
    $especialBD = "S";
} else  {
    $especialBD = "N";
}
if (!empty($vendidos)){
    $vendidoBD = "S";
} else  {
    $vendidoBD = "N";
}
if (!empty($destaque)){
    $destaqueBD = "S";
} else  {
    $destaqueBD = "N";
}
if (!empty($novo)){
    $novoBD = "S";
} else  {
    $novoBD = "";
}


if ($_SESSION["nivel"] == "admin"){
    
    $edita = new ManipulateData();
    $edita->setTable("produto");
    $edita->setCamposBanco("id_marca_produto='$secMarca',id_grupo_produto='$secGrupo',nome_produto='$inputProduto',cod_produto='$inputCod',preco_produto='$inputPreco',preco_desconto_produto='$precoDesconto',porcent_desconto_produto='$porcentDesconto',descricao_produto='$textAreaDescricao',visao_geral_produto='$textAreaVisGeral',info_adicional_produto='$textAreaInfoAdicinal',link_video_produto='$inputLinkVideo',destaque_produto='$destaqueBD',especial_produto='$especialBD',mais_vendido_produto='$vendidoBD',novo_produto='$novoBD'");
    $edita->setFieldId("id_produto");
    $edita->setValueId("$idProduto");
    $edita->update();
    
    $_SESSION["erroImagem"] = "editado";
    header("location: ../../produtosCadastrados.php");
    
} else {
    header("location: ../../accessDanied.php");
}
