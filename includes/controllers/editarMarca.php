<?php

require_once '../models/ManipulateData.php';

/*
 * Editar MARCA
 */

session_start();

$idEdMarca = addslashes($_POST["idEdMarca"]);
$marca = addslashes($_POST["inputMarca"]);
$descricaoMarca = addslashes($_POST["textAreaDescMarca"]);

if (!empty($marca)) {
    
    $editarMar = new ManipulateData();
    $editarMar->setTable("marca_produto");
    $editarMar->setFieldId("id_marca_produto");
    $editarMar->setValueId($idEdMarca);
    $editarMar->setCamposBanco("nome_marca='$marca',descricao_marca='$descricaoMarca'");
    $editarMar->update();
    
    $_SESSION["erroMarca"] = "editado";
    header("location: ../../marcasCadastradas.php");
    
} else {
    header("location: ../../erro.php");
}