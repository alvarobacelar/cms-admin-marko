<?php

require_once '../models/ManipulateData.php';

session_start();

if (isset($_POST["idEd"])) {

    $idEd = addslashes($_POST["idEd"]);
    $categoria = addslashes($_POST["inputCategoria"]);

    $editarCat = new ManipulateData();
    $editarCat->setTable("categoria_produto");
    $editarCat->setFieldId("id_categoria_produto");
    $editarCat->setValueId($idEd);
    $editarCat->setCamposBanco("categoria_nome='$categoria'");
    $editarCat->update();
    
    $_SESSION["erroCategoria"] = "editado";
    header("location: ../../categoriasCadastradas.php");
    
} else {
    header("location: ../../erro.php");
}