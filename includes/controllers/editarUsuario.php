<?php

require_once '../models/ManipulateData.php';
session_start();

//CAPTANDO DADOS DO FORMULARIO
$idUser = addslashes($_POST["idUser"]);
$nome = addslashes($_POST["inputNome"]);
$login = addslashes($_POST["inputLogin"]);
$nivel = addslashes($_POST["selectNivel"]);
$senha = addslashes($_POST["inputSenha"]);
$senha2 = addslashes($_POST["inputSenha2"]);

if ($senha == $senha2) {

    $ediUser = new ManipulateData();
    $ediUser->setTable("usuario");
    $ediUser->setFieldId("id_usuario");
    $ediUser->setValueId($idUser);

    if (!empty($senha)) {
        $senhaCryp = md5($senha);
        $ediUser->setCamposBanco("nome_usuario='$nome',login_usuario='$login',nivel_usuario='$nivel',senha_usuario='$senhaCryp'");
    } else {
        $ediUser->setCamposBanco("nome_usuario='$nome',login_usuario='$login',nivel_usuario='$nivel'");
    }
    $ediUser->update();
    $_SESSION["erroUser"] = "editado";
    header("location: ../../gerenciarUser.php");
} else {
    $_SESSION["erroUser"] = "As senhas não conferem";
    header("location: ../../gerenciarUser.php");
}