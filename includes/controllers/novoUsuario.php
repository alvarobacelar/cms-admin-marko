<?php

require_once '../models/ManipulateData.php';
/*
 * CADASTROS
 */

//CAPTANDO DADOS DO FORMULARIO
$nome = addslashes($_POST["inputNome"]);
$login = addslashes($_POST["inputLogin"]);
$nivel = addslashes($_POST["selectNivel"]);
$senha = addslashes($_POST["inputSenha"]);
$senha2 = addslashes($_POST["inputSenha2"]);
$data = date("Y-m-d");

session_start();
if ($_SESSION["nivel"] == "admin") {

    if ($senha == $senha2) {

        $senha = md5($senha);
        //INSTACIANDO O OBJETO DE CADASTRO
        $cad = new ManipulateData(); //INSTACIANDO A CLASSE
        $cad->setTable("usuario"); //SETANDO O NOME DA TABELA
        $cad->setCampoTable("login_usuario");

        //VERIFICANDO SE EXISTE REGISTRO CADASTRADO
        if ($cad->getDadosDuplicados("$login") >= 1) {
            $_SESSION["erroUser"] = "duplicado";
            header("Location: ../../cadastrarUser.php");
        } else {
            $cad->setCamposBanco("nome_usuario,login_usuario,senha_usuario,nivel_usuario, status_usuario, data_cadastro_usuario"); //CAMPOS DO BANCO DE DADOS
            $cad->setDados("'$nome', '$login', '$senha', '$nivel', 'A', '$data'"); //DADOS DO FORMULARIOS
            $cad->insert(); //EFETUANDO CADASTRO
            $_SESSION["erro"] = "OK";
            header("location: ../../cadastrarUser.php");
        }
    } else {
        $_SESSION["erro"] = "erro";
        header("location: ../../cadastrarUser.php");
    }
} else {
    header("location: ../../accessDenied.php");
}