<?php

session_start();
require_once '../models/ManipulateData.php';

if (isset($_GET["idPrd"])) {
    $idPr = addslashes($_GET["idPrd"]);
    $status = addslashes($_GET["status"]);
    
    $disp = new ManipulateData();
    $disp->setTable("produto");
    $disp->setFieldId("id_produto");
    $disp->setValueId($idPr);
    $disp->setCamposBanco("disponibilidade_produto = '$status'");
    $disp->update();
    
    $_SESSION["erroImagem"] = "disponivel";
    header("location: ../../produtosCadastrados.php");
} else {
    $_SESSION["erroImagem"] = "Parametro de inválido";
    header("location: ../../produtosCadastrados.php");
}
