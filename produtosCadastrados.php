<?php

require_once './libs/smarty/config/config.php';
require_once './includes/models/ManipulateData.php';
require_once './includes/funcoes/verifica.php';
require_once './includes/classes/Pagination.php';

if ($estaLogado == "SIM") {

    /*
     * Caso o usuário clique no botão indicado após realizar upload, será destruidos as
     * sessions que leva a página de upload de imagem
     */
    if (isset($_GET["s"])) {
        if ($_GET["s"] == "u") {
            unset($_SESSION["idProdutoNovo"]);
            unset($_SESSION["erroProduto"]);
        }
    }

    /*
     * Verificando o retorno do cadastro no banco de dados com a session "erroProduto"
     */
    if (isset($_SESSION["erroImagem"])) {
        $erro = $_SESSION["erroImagem"];
        if ($erro == "img") {
            $smarty->assign("erroProduto", "<div class='alert alert-success' role='alert'>Imagem cadastrada com sucesso!</div>");
        } else
        if ($erro == "duplicado") {
            $smarty->assign("erroProduto", "<div class='alert alert-danger' role='alert'>Erro! Produto já cadastrado</div>");
        } else
        if ($erro == "editado") {
            $smarty->assign("erroProduto", "<div class='alert alert-success' role='alert'>Produto Editado!</div>");
        } else
        if ($erro == "excluido") {
            $smarty->assign("erroProduto", "<div class='alert alert-success' role='alert'>Imagens excluidas com sucesso!</div>");
        } else
        if ($erro == "disponivel") {
            $smarty->assign("erroProduto", "<div class='alert alert-success' role='alert'>Status do produto atualizado com sucesso!</div>");
        } else {
            $smarty->assign("erroProduto", "<div class='alert alert-danger' role='alert'>Erro! " . $_SESSION["erroProduto"] . " </div>");
        }
    } else {
        $smarty->assign("erroProduto", "");
    }
    unset($_SESSION["erroImagem"]);

    /*
     * Setando os parâmetros básicos para buscar o produto
     */
    $prodCad = new ManipulateData();
    $prodCad->setTable("produto, grupo_produto,marca_produto");
    // se caso o usuário estiver pesquisando por algum produto, será mostrado apenas o produto da pesquisa
    if (isset($_GET["bp"])) {
        $busca = addslashes($_GET["bp"]);
        $prodCad->setOrderTable("WHERE produto.id_marca_produto = marca_produto.id_marca_produto AND "
                . "produto.id_grupo_produto = grupo_produto.id_grupo_produto AND produto.nome_produto like '%$busca%' ORDER BY id_produto DESC");
        $smarty->assign("busca", "$busca");
        $local = "<li><a href='./'>Painel Incial</a></li>
            <li><a href='./produtosCadastrados.php'>Produtos Cadastrados</a></li>
            <li class='active'>Busca por produtos</li>";
        // caso a página não venha da busca de produto, serão mostrados todos os produtos    
    } else {

        // ########### PAGINAÇÃO #############//
        $paginacao = new ManipulateData();
        if (isset($_GET["pg"])) { // se exitir uma variavel na URL é setado para a paginação
            $pg = $_GET["pg"];
        } else {
            $pg = 1;
        }
        $quantProduto = 20; // Quantidade de chamado por pagina
        $inicio = ($pg * $quantProduto) - $quantProduto; // Definindo o inicio da paginação
        // ######### FIM DA PAGINAÇÃO ###########// 

        $prodCad->setOrderTable("WHERE produto.id_marca_produto = marca_produto.id_marca_produto AND "
                . "produto.id_grupo_produto = grupo_produto.id_grupo_produto ORDER BY id_produto DESC LIMIT $inicio, $quantProduto");
        $smarty->assign("busca", "");
        $local = "<li><a href='./'>Painel Incial</a></li>
            <li class='active'>Produtos Cadastrados</li>";
        $pagina = new Pagination($pg, $quantProduto, $paginacao->CountProdutosTodos());
    }
    $prodCad->select();

    $imagemCad = new ManipulateData();
    $imagemCad->setTable("imagem_produto");
    while ($dbProduto = $prodCad->fetch_object()) {
        $idProduto[] = $dbProduto->id_produto;
        $produto[] = $dbProduto->nome_produto;
        $codProduto[] = $dbProduto->cod_produto;
        $preco[] = $dbProduto->preco_produto;
        $grupoProduto[] = $dbProduto->nome_grupo;
        $marcaProduto[] = $dbProduto->nome_marca;
        $dataCadastro[] = $dbProduto->data_cadastro_produto;
        $disponivel[] = $dbProduto->disponibilidade_produto;

        $smarty->assign("disponibilidade_produto", $disponivel);
        $smarty->assign("id_produto", $idProduto);
        $smarty->assign("nome_produto", $produto);
        $smarty->assign("cod_produto", $codProduto);
        $smarty->assign("preco_produto", $preco);
        $smarty->assign("nome_grupo", $grupoProduto);
        $smarty->assign("nome_marca", $marcaProduto);
        $smarty->assign("data_cadastro_produto", $dataCadastro);

        $imagemCad->setOrderTable("WHERE id_produto = '$dbProduto->id_produto' ");
        $imagemProdutoCad[] = $imagemCad->countTotal();
        $smarty->assign("imagemProduto", $imagemProdutoCad);
    }

    /*
     * Setando os parâmetros básicos do smarty para a págin produtos cadastrados 
     */
    $smarty->assign("paginacao", $pagina->paginacao());
    $smarty->assign("local", $local);
    $smarty->assign("titulo", "Produtos Cadastrados - Marko");
    $smarty->assign("conteudo", "paginas/produtosCadastrado.tpl");
    $smarty->display("layout.tpl");
}