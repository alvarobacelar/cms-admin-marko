<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 01:04:13
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gruposCadastrados.tpl" */ ?>
<?php /*%%SmartyHeaderCode:148068088056a9852d22ea10-43869094%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3c6cd9ca1c28dc64ca6a16c9b084b1b7444c5528' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gruposCadastrados.tpl',
      1 => 1453866191,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '148068088056a9852d22ea10-43869094',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'erroGrupo' => 0,
    'produto' => 0,
    'p' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a9852d25b412_99088834',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a9852d25b412_99088834')) {function content_56a9852d25b412_99088834($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/www/html/siteMarko/admin/libs/smarty/lib/plugins/modifier.date_format.php';
?><h2 class="sub-header">Grupos cadastrados <a href="gerenciarGrupo.php" type="button" class="btn btn-primary btn-xs"> Novo</a></h2>
<div class="table-responsive">
    <?php echo $_smarty_tpl->tpl_vars['erroGrupo']->value;?>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome grupo</th>
                <th>Categoria do grupo</th>
                <th>Data cadastro</th>
                <th>Status</th>
                <th><center>Ações</center></th>
            </tr>
        </thead>
        <tbody>
            <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['produto']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value){
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['p']->value->id_grupo_produto;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['p']->value->nome_grupo;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['p']->value->categoria_nome;?>
</td>
                    <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['p']->value->data_cadastro_grupo,"%d/%m/%Y");?>
</td>
                    <td><?php if ($_smarty_tpl->tpl_vars['p']->value->status_grupo=="A"){?> <span class="text-success"><strong>Ativo</strong></span> <?php }else{ ?> <span class="text-danger"><strong>Desativado</strong></span> <?php }?></td>
                    <td width="150">
                        <a href="editarGrupo.php?idEdit=<?php echo $_smarty_tpl->tpl_vars['p']->value->id_grupo_produto;?>
" class="btn btn-warning btn-xs">Editar</a>
                        <a class="btn btn-danger btn-xs">Desativar</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div><?php }} ?>