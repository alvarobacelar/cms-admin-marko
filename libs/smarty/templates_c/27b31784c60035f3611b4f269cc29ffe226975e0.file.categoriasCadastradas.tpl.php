<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 01:21:45
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/categoriasCadastradas.tpl" */ ?>
<?php /*%%SmartyHeaderCode:107444002256a984221a1120-50732610%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '27b31784c60035f3611b4f269cc29ffe226975e0' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/categoriasCadastradas.tpl',
      1 => 1453951184,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '107444002256a984221a1120-50732610',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a984221c0204_13634703',
  'variables' => 
  array (
    'erroCategoria' => 0,
    'categoria' => 0,
    'c' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a984221c0204_13634703')) {function content_56a984221c0204_13634703($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/www/html/siteMarko/admin/libs/smarty/lib/plugins/modifier.date_format.php';
?><h2 class="sub-header">Categorias cadastradas <a href="gerenciarCategoria.php" type="button" class="btn btn-primary btn-xs"> Novo</a></h2> 
<div class="table-responsive">
    <?php echo $_smarty_tpl->tpl_vars['erroCategoria']->value;?>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome Categoria</th>
                <th>Data Cadastro</th>
                <th>Status</th>
                <th>Opções</th>
            </tr>
        </thead>
        <tbody>
            <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categoria']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['c']->value->id_categoria_produto;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['c']->value->categoria_nome;?>
</td>
                    <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['c']->value->data_cadastro,"%d/%m/%Y");?>
</td>
                    <td><?php if ($_smarty_tpl->tpl_vars['c']->value->status_categoria=="A"){?> <span class="text-success"><strong>Ativo</strong></span> <?php }else{ ?> Desativado <?php }?></td>
                    <td width="150">
                        <a href="editarCategoria.php?idEd=<?php echo $_smarty_tpl->tpl_vars['c']->value->id_categoria_produto;?>
" class="btn btn-warning btn-xs">Editar</a>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['c']->value->id_categoria_produto;?>
" class="btn btn-danger btn-xs">Desativar</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div><?php }} ?>