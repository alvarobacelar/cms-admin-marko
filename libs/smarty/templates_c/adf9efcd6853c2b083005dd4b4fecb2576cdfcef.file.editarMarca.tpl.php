<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 10:45:57
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarMarca.tpl" */ ?>
<?php /*%%SmartyHeaderCode:95148787156aa0bf6019833-63381242%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'adf9efcd6853c2b083005dd4b4fecb2576cdfcef' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarMarca.tpl',
      1 => 1453985152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '95148787156aa0bf6019833-63381242',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56aa0bf602cae4_18197053',
  'variables' => 
  array (
    'marcaEdit' => 0,
    'erroMarca' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56aa0bf602cae4_18197053')) {function content_56aa0bf602cae4_18197053($_smarty_tpl) {?><h1 class="page-header">Editar Marca</h1>

<div class="row placeholders">
    <?php if (!empty($_smarty_tpl->tpl_vars['marcaEdit']->value)){?>
        <?php echo $_smarty_tpl->tpl_vars['erroMarca']->value;?>

        <form action="includes/controllers/editarMarca.php" method="post" id="formCadastrarCategoria" name="formCadastrarCategoria" class="form-horizontal" role="form">

            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputMarca">Marca</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" id="inputMarca" name="inputMarca" required="" value="<?php echo $_smarty_tpl->tpl_vars['marcaEdit']->value->nome_marca;?>
" placeholder="EX: Dell">
                </div>
                <input type="hidden" id="idEdMarca" name="idEdMarca" value="<?php echo $_smarty_tpl->tpl_vars['marcaEdit']->value->id_marca_produto;?>
">
            </div>

            <div class="form-group">
                <label for="textAreaDescMarca" class="col-sm-2 control-label">Descrição</label>
                <div class="col-sm-6 text-left" >
                    <textarea class="form-control" id="textAreaDescMarca" name="textAreaDescMarca" rows="3"><?php echo $_smarty_tpl->tpl_vars['marcaEdit']->value->descricao_marca;?>
</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-1">
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </div>

        </form>
    <?php }else{ ?>
        <div class="alert alert-danger text-center" role="alert"><strong>Marca não existente</strong></div>
    <?php }?>

</div>

<?php }} ?>