<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 01:10:35
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/produtosCadastrado.tpl" */ ?>
<?php /*%%SmartyHeaderCode:63348400756a986ab04cfa8-22463802%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbc94edb0cd15f9ee023d8cdce097823444f9317' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/produtosCadastrado.tpl',
      1 => 1453766969,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '63348400756a986ab04cfa8-22463802',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'busca' => 0,
    'cod_produto' => 0,
    'erroProduto' => 0,
    'nome_produto' => 0,
    'preco_produto' => 0,
    'nome_grupo' => 0,
    'nome_marca' => 0,
    'data_cadastro_produto' => 0,
    'imagemProduto' => 0,
    'id_produto' => 0,
    'disponibilidade_produto' => 0,
    'paginacao' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a986ab1263c0_63416355',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a986ab1263c0_63416355')) {function content_56a986ab1263c0_63416355($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/www/html/siteMarko/admin/libs/smarty/lib/plugins/modifier.date_format.php';
?><h2 class="sub-header">
    <?php if ($_smarty_tpl->tpl_vars['busca']->value==''){?>
        Produtos cadastrados <a href="gerenciarProduto.php" type="button" class="btn btn-primary btn-xs"> Novo</a>
    <?php }else{ ?>
        Busca por: <?php echo $_smarty_tpl->tpl_vars['busca']->value;?>

    <?php }?>
</h2>
<div class="table-responsive">
    <?php if (!empty($_smarty_tpl->tpl_vars['cod_produto']->value)){?>
        <?php echo $_smarty_tpl->tpl_vars['erroProduto']->value;?>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Cod</th>
                    <th>Nome Produto</th>
                    <th>Preço</th>
                    <th>Grupo Produto</th>
                    <th>Marca Produto</th>
                    <th>Data cadastro</th>
                    <th>Imagens</th>
                    <th>Status</th>
                    <th><center>Ações</center></th>
            </tr>
            </thead>
            <tbody>
                <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['p'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['p']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['cod_produto']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['name'] = 'p';
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['p']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['p']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['p']['total']);
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['cod_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['nome_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['preco_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['nome_grupo']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['nome_marca']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
</td>
                        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['data_cadastro_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']],"%d/%m/%Y");?>
</td>
                        <td class="text-center" width="150">
                            <?php if ($_smarty_tpl->tpl_vars['imagemProduto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']]==0){?> 
                                <a href="gerenciarProduto.php?id=<?php echo $_smarty_tpl->tpl_vars['id_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
" class="btn btn-info btn-xs">Cadastrar</a> 
                            <?php }else{ ?> 
                                <span class="badge">
                                    <?php echo $_smarty_tpl->tpl_vars['imagemProduto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
 imagens
                                </span><br>
                                <a href="gerenciarProduto.php?id=<?php echo $_smarty_tpl->tpl_vars['id_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
" class="btn btn-warning btn-xs">Adicionar</a> 
                                <button onclick="excluirImagem(<?php echo $_smarty_tpl->tpl_vars['id_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
)" class="btn btn-danger btn-xs">Excluir</button>
                            <?php }?>
                        </td>
                        <td>
                            <?php if ($_smarty_tpl->tpl_vars['disponibilidade_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']]=="S"){?> 
                                <span class="text-success">
                                    <i><strong>Disponível</strong></i>
                                </span> 
                                <button onclick="dispProduto(<?php echo $_smarty_tpl->tpl_vars['id_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
,'N')" class="btn btn-danger btn-xs">Tornar Indisponível</button>
                            <?php }else{ ?> 
                                <span class="text-danger">
                                    <strong>Indisponível</strong>
                                </span>
                                <button onclick="dispProduto(<?php echo $_smarty_tpl->tpl_vars['id_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
,'S')" class="btn btn-success btn-xs">Tornar Disponível</button>
                            <?php }?>
                        </td>
                        <td>
                            
                            <a href="editarProduto.php?editar=<?php echo $_smarty_tpl->tpl_vars['id_produto']->value[$_smarty_tpl->getVariable('smarty')->value['section']['p']['index']];?>
" title="Ao clicar irá mostrar no site" class="btn btn-info btn-xs">Editar</a>
                        </td>
                    </tr>
                <?php endfor; endif; ?>
            </tbody>
        </table>
        <?php if (!empty($_smarty_tpl->tpl_vars['paginacao']->value)){?>
            <?php echo $_smarty_tpl->tpl_vars['paginacao']->value;?>

        <?php }?>
    <?php }else{ ?>
        <div class="alert alert-danger text-center" role="alert"><strong>Nada foi encontrado, tente buscar novamente.</strong></div>
    <?php }?>
</div><?php }} ?>