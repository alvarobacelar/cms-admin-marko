<?php /* Smarty version Smarty-3.1.13, created on 2016-02-01 12:55:42
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/marcasCadastradas.tpl" */ ?>
<?php /*%%SmartyHeaderCode:152579635556a98a4458b3f3-68864071%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6d6ea8811179b04744a6713da34e71d7e136cb9b' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/marcasCadastradas.tpl',
      1 => 1453985099,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '152579635556a98a4458b3f3-68864071',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a98a445d3f72_73586730',
  'variables' => 
  array (
    'erroMarca' => 0,
    'marcas' => 0,
    'c' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a98a445d3f72_73586730')) {function content_56a98a445d3f72_73586730($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/www/html/siteMarko/admin/libs/smarty/lib/plugins/modifier.date_format.php';
?><h2 class="sub-header">Marcas cadastradas <a href="gerenciarMarca.php" type="button" class="btn btn-primary btn-xs"> Novo</a></h2> 
<div class="table-responsive">
    <?php echo $_smarty_tpl->tpl_vars['erroMarca']->value;?>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome Marca</th>
                <th>Data Cadastro</th>
                <th>Status</th>
                <th>Opções</th>
            </tr>
        </thead>
        <tbody>
            <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['marcas']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['c']->value->id_marca_produto;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['c']->value->nome_marca;?>
</td>
                    <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['c']->value->data_cadastro_marca,"%d/%m/%Y");?>
</td>
                    <td><?php if ($_smarty_tpl->tpl_vars['c']->value->status_marca=="A"){?> <span class="text-success"><strong>Ativo</strong></span> <?php }else{ ?> Desativado <?php }?></td>
                    <td width="150">
                        <a href="editarMarca.php?idEdMar=<?php echo $_smarty_tpl->tpl_vars['c']->value->id_marca_produto;?>
" class="btn btn-warning btn-xs">Editar</a>
                        <a href="<?php echo $_smarty_tpl->tpl_vars['c']->value->id_marca_produto;?>
" class="btn btn-danger btn-xs">Desativar</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div><?php }} ?>