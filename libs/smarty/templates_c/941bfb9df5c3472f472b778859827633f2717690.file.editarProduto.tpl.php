<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 01:12:59
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarProduto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:131683741956a98702c74f40-13403321%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '941bfb9df5c3472f472b778859827633f2717690' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarProduto.tpl',
      1 => 1453950777,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '131683741956a98702c74f40-13403321',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a98702cd7cc7_49007950',
  'variables' => 
  array (
    'prodEdit' => 0,
    'grupo' => 0,
    'produto' => 0,
    'p' => 0,
    'marca' => 0,
    'm' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a98702cd7cc7_49007950')) {function content_56a98702cd7cc7_49007950($_smarty_tpl) {?><h1 class="page-header">Editar Produto</h1>

<div class="row placeholders">

    <?php if (!empty($_smarty_tpl->tpl_vars['prodEdit']->value)){?>

        <form action="includes/controllers/editarProduto.php" method="post" id="formEditarProduto" name="formEditarProduto" class="form-horizontal" role="form">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="selectGrupo">Grupo</label>
                <div class="col-sm-3">
                    <select class="form-control" id="selectGrupo" name="selectGrupo" required="">
                        <option value="<?php echo $_smarty_tpl->tpl_vars['grupo']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->nome_grupo;?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['produto']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value){
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['p']->value->id_grupo_produto;?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value->nome_grupo;?>
</option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="selectMarca">Marca</label>
                <div class="col-sm-3">
                    <select class="form-control" id="selectMarca" name="selectMarca" required="">
                        <option value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->id_marca_produto;?>
"><?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->nome_marca;?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['m'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['m']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['marca']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['m']->key => $_smarty_tpl->tpl_vars['m']->value){
$_smarty_tpl->tpl_vars['m']->_loop = true;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['m']->value->id_marca_produto;?>
"><?php echo $_smarty_tpl->tpl_vars['m']->value->nome_marca;?>
</option>
                        <?php } ?>
                    </select>
                </div>
                <input type="hidden" name="inputIdProduto" id="inputIdProduto" value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->id_produto;?>
">
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputProduto">Nome produto</label>
                <div class="col-sm-8">
                    <input class="form-control" type="text" id="inputProduto" name="inputProduto" required="" value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->nome_produto;?>
" placeholder="EX: Impressora HP ...">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputCod">Código</label>
                <div class="col-sm-3">
                    <input class="form-control" type="text" id="inputCod" name="inputCod" required="" value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->cod_produto;?>
" placeholder="EX: 1000999">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputPreco">Preço</label>
                <div class="col-sm-2">
                    <input class="form-control" type="text" id="inputPreco" name="inputPreco" required="" value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->preco_produto;?>
" placeholder="EX: 400,00">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputPrecoDesconto">Preço Desconto</label>
                <div class="col-sm-2">
                    <input class="form-control" type="text" id="inputPrecoDesconto" name="inputPrecoDesconto" value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->preco_desconto_produto;?>
" placeholder="EX: 320,00">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputPorcentDesconto">Porcentagem do Desconto</label>
                <div class="col-sm-2">
                    <input class="form-control" type="text" id="inputPorcentDesconto" name="inputPorcentDesconto" value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->porcent_desconto_produto;?>
" placeholder="EX: -30%">
                </div>
            </div>

            <div class="form-group">
                <label for="textAreaDescricao" class="col-sm-2 control-label">Descrição</label>
                <div class="col-sm-8 text-left" >
                    <textarea class="form-control" id="textAreaDescricao" name="textAreaDescricao" rows="3"><?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->descricao_produto;?>
</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="textAreaVisGeral" class="col-sm-2 control-label">Visão geral</label>
                <div class="col-sm-8 text-left">
                    <textarea class="form-control" id="textAreaVisGeral" name="textAreaVisGeral" rows="3"><?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->visao_geral_produto;?>
</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="textAreaInfoAdicinal" class="col-sm-2 control-label">Informação adicional</label>
                <div class="col-sm-8 text-left">
                    <textarea class="form-control" id="textAreaInfoAdicinal" name="textAreaInfoAdicinal" rows="3"><?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->info_adicional_produto;?>
</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputLinkVideo">Link Vídeo</label>
                <div class="col-sm-5">
                    <input class="form-control" type="text" id="inputLinkVideo" name="inputLinkVideo" value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->link_video_produto;?>
" placeholder="EX: https://www.youtube.com/watch?v=LVBW5IimwCU">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputLinkVideo">Opções</label>
                <div class="col-sm-offset-0 col-sm-6">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="chekDestaque" id="chekDestaque" <?php if ($_smarty_tpl->tpl_vars['prodEdit']->value->destaque_produto=="S"){?>checked=""<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->destaque_produto;?>
"> Destaque
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="chekEspeciais" id="chekEspeciais" <?php if ($_smarty_tpl->tpl_vars['prodEdit']->value->especial_produto=="S"){?>checked=""<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->especial_produto;?>
"> Especial
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="chekMaisVendidos" id="chekMaisVendidos" <?php if ($_smarty_tpl->tpl_vars['prodEdit']->value->mais_vendido_produto=="S"){?>checked=""<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['prodEdit']->value->mais_vendido_produto;?>
"> Mais vendido
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="chekNovo" id="chekNovo" <?php if ($_smarty_tpl->tpl_vars['prodEdit']->value->novo_produto=="S"){?>checked=""<?php }?> value="S"> Etiqueta Novo
                    </label>
                </div>
            </div>
            <br>
            <div class = "form-group">
                <div class = "col-sm-offset-2 col-sm-1">
                    <button type = "submit" class = "btn btn-success"> Editar </button>
                </div>
            </div>

        </form>
    <?php }else{ ?>
        <div class="alert alert-danger text-center" role="alert"><strong>Produto não existente</strong></div>
    <?php }?>

</div>

<?php }} ?>