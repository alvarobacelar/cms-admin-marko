<?php /* Smarty version Smarty-3.1.13, created on 2016-02-01 12:55:39
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarGrupo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:140621531956a986c0ad6280-71116094%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4c83b61303eff8abf89ef795326c6a53b89b5e6b' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarGrupo.tpl',
      1 => 1454032118,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '140621531956a986c0ad6280-71116094',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a986c0b1ef09_33208981',
  'variables' => 
  array (
    'dbEdit' => 0,
    'erroGrupo' => 0,
    'categoria' => 0,
    'c' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a986c0b1ef09_33208981')) {function content_56a986c0b1ef09_33208981($_smarty_tpl) {?><h1 class="page-header">Editar Grupo</h1>

<div class="row placeholders">

    <?php if (!empty($_smarty_tpl->tpl_vars['dbEdit']->value)){?>

        <?php echo $_smarty_tpl->tpl_vars['erroGrupo']->value;?>

        <form action="includes/controllers/editarGrupo.php" enctype="multipart/form-data" method="post" id="formCadastrarGrupo" name="formCadastrarGrupo" class="form-horizontal" role="form">

            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputGrupo">Nome Grupo</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" id="inputGrupo" name="inputGrupo" required="" value="<?php echo $_smarty_tpl->tpl_vars['dbEdit']->value->nome_grupo;?>
" placeholder="EX: Notebooks">
                </div>
                <input type="hidden" name="idGrupo" id="idGrupo" value="<?php echo $_smarty_tpl->tpl_vars['dbEdit']->value->id_grupo_produto;?>
" >
            </div>

            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="selectCategoria">Categoria</label>
                <div class="col-sm-3">
                    <select class="form-control" id="selectNivel" name="selectCategoria" required="">
                        <option value="<?php echo $_smarty_tpl->tpl_vars['dbEdit']->value->id_categoria_produto;?>
"><?php echo $_smarty_tpl->tpl_vars['dbEdit']->value->categoria_nome;?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categoria']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
                            <option value="<?php echo $_smarty_tpl->tpl_vars['c']->value->id_categoria_produto;?>
"><?php echo $_smarty_tpl->tpl_vars['c']->value->categoria_nome;?>
</option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="textAreaDescricaoGp" class="col-sm-2 control-label">Descrição Grupo</label>
                <div class="col-sm-8 text-left" >
                    <textarea class="form-control" id="textAreaDescricaoGp" name="textAreaDescricaoGp"><?php echo $_smarty_tpl->tpl_vars['dbEdit']->value->descricao_grupo;?>
</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="fileGrupo">Imagem do Grupo</label>
                <div class="col-sm-6">
                    <input class="form-control" type="file" id="fileGrupo" value="" name="fileGrupo" ><br />
                </div>
            </div>  

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-1">
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </div>

        </form>
    <?php }else{ ?>
        <div class="alert alert-danger text-center" role="alert"><strong>Grupo não existente</strong></div>
    <?php }?>

</div>
<?php }} ?>