<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 01:17:26
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarCategoria.tpl" */ ?>
<?php /*%%SmartyHeaderCode:67401026756a984990e9998-30393775%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e954357342e452f59a96033867ae1d46005c4093' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarCategoria.tpl',
      1 => 1453951041,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '67401026756a984990e9998-30393775',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a984990fa719_57769767',
  'variables' => 
  array (
    'catEdit' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a984990fa719_57769767')) {function content_56a984990fa719_57769767($_smarty_tpl) {?><h1 class="page-header">Editar Categoria</h1>

<div class="row placeholders">

    <?php if (!empty($_smarty_tpl->tpl_vars['catEdit']->value)){?>
        <form action="includes/controllers/editarCategoria.php" method="post" id="formCadastrarCategoria" name="formCadastrarCategoria" class="form-horizontal" role="form">

            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputCategoria">Categoria</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" id="inputCategoria" name="inputCategoria" required="" value="<?php echo $_smarty_tpl->tpl_vars['catEdit']->value->categoria_nome;?>
" placeholder="EX: Informática">
                </div>
                <input type="hidden" id="idEd" name="idEd" value="<?php echo $_smarty_tpl->tpl_vars['catEdit']->value->id_categoria_produto;?>
" >
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-1">
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </div>

        </form>
    <?php }else{ ?>
        <div class="alert alert-danger text-center" role="alert"><strong>Categoria não existente</strong></div>
    <?php }?>

</div>

<?php }} ?>