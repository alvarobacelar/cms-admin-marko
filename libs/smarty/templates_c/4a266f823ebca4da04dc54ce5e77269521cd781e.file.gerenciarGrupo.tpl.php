<?php /* Smarty version Smarty-3.1.13, created on 2016-02-01 12:53:06
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gerenciarGrupo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:80192709356af7f6284c922-72107115%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4a266f823ebca4da04dc54ce5e77269521cd781e' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gerenciarGrupo.tpl',
      1 => 1453610446,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '80192709356af7f6284c922-72107115',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'erroGrupo' => 0,
    'categoria' => 0,
    'c' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56af7f628bf640_80174790',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56af7f628bf640_80174790')) {function content_56af7f628bf640_80174790($_smarty_tpl) {?><h1 class="page-header">Cadastrar Grupo</h1>

<div class="row placeholders">

    <?php echo $_smarty_tpl->tpl_vars['erroGrupo']->value;?>

    <form action="includes/controllers/cadastrarGrupo.php" enctype="multipart/form-data" method="post" id="formCadastrarGrupo" name="formCadastrarGrupo" class="form-horizontal" role="form">

        <div class="form-group form-group-sm">
            <label class="col-sm-2 control-label" for="inputGrupo">Novo Grupo</label>
            <div class="col-sm-4">
                <input class="form-control" type="text" id="inputGrupo" name="inputGrupo" required="" value="" placeholder="EX: Notebooks">
            </div>
        </div>

        <div class="form-group form-group-sm">
            <label class="col-sm-2 control-label" for="selectCategoria">Categoria</label>
            <div class="col-sm-3">
                <select class="form-control" id="selectNivel" name="selectCategoria" required="">
                    <option value="">Categoria do grupo</option>
                    <?php  $_smarty_tpl->tpl_vars['c'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['c']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['categoria']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['c']->key => $_smarty_tpl->tpl_vars['c']->value){
$_smarty_tpl->tpl_vars['c']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['c']->value->id_categoria_produto;?>
"><?php echo $_smarty_tpl->tpl_vars['c']->value->categoria_nome;?>
</option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="textAreaDescricaoGp" class="col-sm-2 control-label">Descrição Grupo</label>
            <div class="col-sm-8 text-left" >
                <textarea class="form-control" id="textAreaDescricaoGp" name="textAreaDescricaoGp"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="fileGrupo">Imagem do Grupo</label>
            <div class="col-sm-6">
                <input class="form-control" type="file" id="fileGrupo" value="" name="fileGrupo"required=""><br />
            </div>
        </div>  

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-1">
                <button type="submit" class="btn btn-success">Cadastrar</button>
            </div>
        </div>

    </form>

</div>
<?php }} ?>