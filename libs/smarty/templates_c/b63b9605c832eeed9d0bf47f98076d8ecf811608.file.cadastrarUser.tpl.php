<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 11:05:09
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/cadastrarUser.tpl" */ ?>
<?php /*%%SmartyHeaderCode:132380528956aa0d93190dd4-92339621%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b63b9605c832eeed9d0bf47f98076d8ecf811608' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/cadastrarUser.tpl',
      1 => 1453986306,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '132380528956aa0d93190dd4-92339621',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56aa0d931ad8f8_06826171',
  'variables' => 
  array (
    'erroUser' => 0,
    'user' => 0,
    'u' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56aa0d931ad8f8_06826171')) {function content_56aa0d931ad8f8_06826171($_smarty_tpl) {?><div class="panel panel-default">

    <div class="panel-heading">

        <h2 class="panel-title">Cadastrar novo usuário</h2>
    </div>
    <div class="panel-body">

        <?php echo $_smarty_tpl->tpl_vars['erroUser']->value;?>
 
        <form action="includes/controllers/novoUsuario.php" method="post" name="cadastrar" class="form-horizontal" role="form" onSubmit="return verificaSenha()">
            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputNome">Nome Completo</label>
                <div class="col-sm-6">
                    <input class="form-control" type="text" id="inputNome" name="inputNome" required="" placeholder="Nome do Usuário">
                </div>
            </div>            

            <div class="row form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputLogin">Login</label>
                <div class="col-sm-3">
                    <input class="form-control" type="text" id="inputLogin" name="inputLogin" required="" placeholder="Login do usuário">

                </div>
                <div class="col-sm-3">
                    <select class="form-control" id="selectNivel" name="selectNivel" required="">
                        <option value="">Nivel do usuário</option>
                        <option value="0">Administrador</option>
                        <option value="1">Supervisor</option>
                        <option value="2">Usuário Básico</option>
                    </select>
                </div>
            </div>
            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputSenha">Senha</label>
                <div class="col-sm-3">
                    <input class="form-control" type="password" id="inputSenha" value="" name="inputSenha" required="" placeholder="Senha">
                </div>
            </div>
            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputSenha2">Repita a senha</label>
                <div class="col-sm-3">
                    <input class="form-control" type="password" id="inputSenha2" value="" name="inputSenha2" required="" onblur="verificaSenha()" placeholder="Repitir senha">
                    <span id="erro-senha" class="text-danger"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">Cadastrar</button>
                </div>
            </div>
        </form>
        <br />

    </div>
</div>

<h2 class="sub-header">Usuários Cadastrados</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Login</th>
                <th>Nível</th> 
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            <?php  $_smarty_tpl->tpl_vars['u'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['u']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['u']->key => $_smarty_tpl->tpl_vars['u']->value){
$_smarty_tpl->tpl_vars['u']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['u']->value->id_usuario;?>
</td>
                    <td width="400"><?php echo $_smarty_tpl->tpl_vars['u']->value->nome_usuario;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['u']->value->login_usuario;?>
</td>
                    <td>
                        <?php if ($_smarty_tpl->tpl_vars['u']->value->nivel_usuario==0){?> 
                            Administrador 
                        <?php }elseif($_smarty_tpl->tpl_vars['u']->value->nivel_usuario==1){?>
                            Supervisor
                        <?php }elseif($_smarty_tpl->tpl_vars['u']->value->nivel_usuario==2){?>
                            Usuário Comum
                        <?php }?>
                    </td>
                    <td>
                        <a href="gerenciarUser.php?idUser=<?php echo $_smarty_tpl->tpl_vars['u']->value->id_usuario;?>
" class="btn btn-warning btn-xs">Editar</a>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <center><a class="btn btn-default" href="javascript:history.back()"> Voltar</a></center> <?php }} ?>