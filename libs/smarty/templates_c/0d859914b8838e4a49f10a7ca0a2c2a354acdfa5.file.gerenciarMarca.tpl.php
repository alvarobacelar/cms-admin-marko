<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 10:38:58
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gerenciarMarca.tpl" */ ?>
<?php /*%%SmartyHeaderCode:145117757856aa0be2190796-65004121%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d859914b8838e4a49f10a7ca0a2c2a354acdfa5' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gerenciarMarca.tpl',
      1 => 1452992193,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '145117757856aa0be2190796-65004121',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'erroMarca' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56aa0be219c290_22296534',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56aa0be219c290_22296534')) {function content_56aa0be219c290_22296534($_smarty_tpl) {?><h1 class="page-header">Cadastrar Marca</h1>

<div class="row placeholders">
<?php echo $_smarty_tpl->tpl_vars['erroMarca']->value;?>

    <form action="includes/controllers/cadastrarMarca.php" method="post" id="formCadastrarCategoria" name="formCadastrarCategoria" class="form-horizontal" role="form">

        <div class="form-group form-group-sm">
            <label class="col-sm-2 control-label" for="inputMarca">Nova Marca</label>
            <div class="col-sm-4">
                <input class="form-control" type="text" id="inputMarca" name="inputMarca" required="" value="" placeholder="EX: Dell">
            </div>
        </div>
        
        <div class="form-group">
            <label for="textAreaDescMarca" class="col-sm-2 control-label">Descrição</label>
            <div class="col-sm-6 text-left" >
                <textarea class="form-control" id="textAreaDescMarca" name="textAreaDescMarca" rows="3"></textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-1">
                <button type="submit" class="btn btn-success">Cadastrar</button>
            </div>
        </div>

    </form>

</div>

<?php }} ?>