<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 01:26:06
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gerenciarProduto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:184691668256a98a4ec2e709-58744258%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47a3fbe0e5d0e9011904e12942353e056dbb8c71' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gerenciarProduto.tpl',
      1 => 1453570756,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '184691668256a98a4ec2e709-58744258',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'produto' => 0,
    'p' => 0,
    'marca' => 0,
    'm' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a98a4ec52647_18825058',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a98a4ec52647_18825058')) {function content_56a98a4ec52647_18825058($_smarty_tpl) {?><h1 class="page-header">Cadastrar Produto</h1>

<div class="row placeholders">

    <form action="includes/controllers/cadastrarProduto.php" method="post" id="formCadastrarProduto" name="formCadastrarProduto" class="form-horizontal" role="form">

        <div class="form-group">
            <label class="col-sm-2 control-label" for="selectGrupo">Grupo</label>
            <div class="col-sm-3">
                <select class="form-control" id="selectGrupo" name="selectGrupo" required="">
                    <option value="">Grupo do produto</option>
                    <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['produto']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value){
$_smarty_tpl->tpl_vars['p']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['p']->value->id_grupo_produto;?>
"><?php echo $_smarty_tpl->tpl_vars['p']->value->nome_grupo;?>
</option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="selectMarca">Marca</label>
            <div class="col-sm-3">
                <select class="form-control" id="selectMarca" name="selectMarca" required="">
                    <option value="">Selecione a marca</option>
                    <?php  $_smarty_tpl->tpl_vars['m'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['m']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['marca']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['m']->key => $_smarty_tpl->tpl_vars['m']->value){
$_smarty_tpl->tpl_vars['m']->_loop = true;
?>
                        <option value="<?php echo $_smarty_tpl->tpl_vars['m']->value->id_marca_produto;?>
"><?php echo $_smarty_tpl->tpl_vars['m']->value->nome_marca;?>
</option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputProduto">Nome produto</label>
            <div class="col-sm-8">
                <input class="form-control" type="text" id="inputProduto" name="inputProduto" required="" value="" placeholder="EX: Impressora HP ...">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputCod">Código</label>
            <div class="col-sm-3">
                <input class="form-control" type="text" id="inputCod" name="inputCod" required="" value="" placeholder="EX: 1000999">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPreco">Preço</label>
            <div class="col-sm-2">
                <input class="form-control" type="text" id="inputPreco" name="inputPreco" required="" value="" placeholder="EX: 400,00">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPrecoDesconto">Preço Desconto</label>
            <div class="col-sm-2">
                <input class="form-control" type="text" id="inputPrecoDesconto" name="inputPrecoDesconto" value="" placeholder="EX: 320,00">
            </div>
        </div>
                
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputPorcentDesconto">Porcentagem do Desconto</label>
            <div class="col-sm-2">
                <input class="form-control" type="text" id="inputPorcentDesconto" name="inputPorcentDesconto" value="" placeholder="EX: -30%">
            </div>
        </div>

        <div class="form-group">
            <label for="textAreaDescricao" class="col-sm-2 control-label">Descrição</label>
            <div class="col-sm-8 text-left" >
                <textarea class="form-control" id="textAreaDescricao" name="textAreaDescricao" rows="3"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="textAreaVisGeral" class="col-sm-2 control-label">Visão geral</label>
            <div class="col-sm-8 text-left">
                <textarea class="form-control" id="textAreaVisGeral" name="textAreaVisGeral" rows="3"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="textAreaInfoAdicinal" class="col-sm-2 control-label">Informação adicional</label>
            <div class="col-sm-8 text-left">
                <textarea class="form-control" id="textAreaInfoAdicinal" name="textAreaInfoAdicinal" rows="3"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputLinkVideo">Link Vídeo</label>
            <div class="col-sm-5">
                <input class="form-control" type="text" id="inputLinkVideo" name="inputLinkVideo" value="" placeholder="EX: https://www.youtube.com/watch?v=LVBW5IimwCU">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputLinkVideo">Opções</label>
            <div class="col-sm-offset-0 col-sm-6">
                <label class="checkbox-inline">
                    <input type="checkbox" name="chekDestaque" id="chekDestaque" value="S"> Destaque
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="chekEspeciais" id="chekEspeciais" value="S"> Especial
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="chekMaisVendidos" id="chekMaisVendidos" value="S"> Mais vendido
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="chekNovo" id="chekNovo" value="S"> Etiqueta Novo
                </label>
            </div>
        </div>
        <br>
        <div class = "form-group">
            <div class = "col-sm-offset-2 col-sm-1">
                <button type = "submit" class = "btn btn-success"> Cadastrar </button>
            </div>
        </div>

    </form>

</div>

<?php }} ?>