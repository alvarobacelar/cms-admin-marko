<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 15:38:56
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:190656483856aa083c567db2-86075128%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '921a8c33c0b470c13d7fee241393c282cda1b5d8' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/login.tpl',
      1 => 1454006334,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '190656483856aa083c567db2-86075128',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56aa083c676f50_88525344',
  'variables' => 
  array (
    'titulo' => 0,
    'erro' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56aa083c676f50_88525344')) {function content_56aa083c676f50_88525344($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title><?php echo $_smarty_tpl->tpl_vars['titulo']->value;?>
</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/signin.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">

            <form class="form-signin" action="logSistem.php" method="post" name="logSistem">
                <h2 class="form-signin-heading">
                    <center>Marko Informática<br>
                        <small> Entre com o usuário e senha</small>
                    </center>
                </h2>
                <?php echo $_smarty_tpl->tpl_vars['erro']->value;?>

                <label for="inputLogin" class="sr-only">Login</label>
                <input type="text" id="inputLogin" name="inputLogin" class="form-control" placeholder="Usuário" required autofocus>
                <label for="inputSenha" class="sr-only">Senha</label>
                <input type="password" id="inputSenha" name="inputSenha" class="form-control" placeholder="Senha" required>
                
                <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
            </form>

        </div> <!-- /container -->


        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>
<?php }} ?>