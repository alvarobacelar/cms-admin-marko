<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 01:26:08
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:48992877656a98a5023a199-90082497%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7ef7173cb73adce4ac2040e9aa82e4efb42f189e' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/dashboard.tpl',
      1 => 1453646064,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '48992877656a98a5023a199-90082497',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'prodHome' => 0,
    'ph' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a98a5025f386_58743720',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a98a5025f386_58743720')) {function content_56a98a5025f386_58743720($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/www/html/siteMarko/admin/libs/smarty/lib/plugins/modifier.date_format.php';
?><h1 class="page-header">Painel Inicial</h1>

<div class="row placeholders">
    <div class="col-xs-6 col-sm-3 placeholder">
        <a href="./gerenciarProduto.php">
            <img src="imagens/prod.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
            <h4>Produto</h4>
            <span class="text-muted">Novo Produto</span>
        </a>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <a href="./gerenciarCategoria.php">
            <img src="imagens/categ.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
            <h4>Categoria</h4>
            <span class="text-muted">Nova Categoria</span>
        </a>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <a href="./gerenciarGrupo.php">
            <img src="imagens/grupo.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
            <h4>Grupo</h4>
            <span class="text-muted">Novo Grupo</span>
        </a>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <a href="./gerenciarMarca.php">
            <img src="imagens/marca.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
            <h4>Marca</h4>
            <span class="text-muted">Nova Marca</span>
        </a>
    </div>
</div>

<h2 class="sub-header">Recentes</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Produto</th>
                <th>Preço</th>
                <th>Grupo</th>
                <th>Marca</th>
                <th>Data Cadastro</th>
            </tr>
        </thead>
        <tbody>
            <?php  $_smarty_tpl->tpl_vars['ph'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ph']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prodHome']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ph']->key => $_smarty_tpl->tpl_vars['ph']->value){
$_smarty_tpl->tpl_vars['ph']->_loop = true;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['ph']->value->cod_produto;?>
</td>
                    <td width="400"><?php echo $_smarty_tpl->tpl_vars['ph']->value->nome_produto;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['ph']->value->preco_produto;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['ph']->value->nome_grupo;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['ph']->value->nome_marca;?>
</td>
                    <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['ph']->value->data_cadastro_produto,"%d/%m/%Y");?>
</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <center><a href="produtosCadastrados.php" class="btn btn-default">Veja Mais</a></center>
</div><?php }} ?>