<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 23:18:31
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarUser.tpl" */ ?>
<?php /*%%SmartyHeaderCode:186828083656aa133bba44d2-06328194%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a7995effe5e25e3de0a65ea5fe2384be0fe589f5' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/editarUser.tpl',
      1 => 1454033908,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '186828083656aa133bba44d2-06328194',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56aa133bbddc12_87640541',
  'variables' => 
  array (
    'edUser' => 0,
    'erroUser' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56aa133bbddc12_87640541')) {function content_56aa133bbddc12_87640541($_smarty_tpl) {?><?php if (!empty($_smarty_tpl->tpl_vars['edUser']->value)){?>

    <div class="panel panel-default">

        <div class="panel-heading">

            <h2 class="panel-title">Editar usuário</h2>
        </div>
        <div class="panel-body">

            <?php echo $_smarty_tpl->tpl_vars['erroUser']->value;?>
 
            <form action="includes/controllers/editarUsuario.php" method="post" name="cadastrar" class="form-horizontal" role="form" onSubmit="return verificaSenha()">
                <div class="form-group form-group-sm">
                    <label class="col-sm-2 control-label" for="inputNome">Nome Completo</label>
                    <div class="col-sm-6">
                        <input class="form-control" type="text" id="inputNome" name="inputNome" value="<?php echo $_smarty_tpl->tpl_vars['edUser']->value->nome_usuario;?>
" required="" placeholder="Nome do Usuário">
                    </div>
                    <input type="hidden" id="idUser" name="idUser" value="<?php echo $_smarty_tpl->tpl_vars['edUser']->value->id_usuario;?>
">
                </div>            

                <div class="row form-group form-group-sm">
                    <label class="col-sm-2 control-label" for="inputLogin">Login</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" id="inputLogin" name="inputLogin" value="<?php echo $_smarty_tpl->tpl_vars['edUser']->value->login_usuario;?>
" required="" placeholder="Login do usuário">

                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" id="selectNivel" name="selectNivel" required="">
                            <option value="<?php echo $_smarty_tpl->tpl_vars['edUser']->value->nivel_usuario;?>
">
                                <?php if ($_smarty_tpl->tpl_vars['edUser']->value->nivel_usuario==0){?> 
                                    Administrador 
                                <?php }elseif($_smarty_tpl->tpl_vars['edUser']->value->nivel_usuario==1){?>
                                    Supervisor
                                <?php }elseif($_smarty_tpl->tpl_vars['edUser']->value->nivel_usuario==2){?>
                                    Usuário Comum
                                <?php }?>
                            </option>
                            <option value="0">Administrador</option>
                            <option value="1">Supervisor</option>
                            <option value="2">Usuário Básico</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-2 control-label" for="inputSenha">Senha</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="password" id="inputSenha" value="" name="inputSenha" placeholder="Senha">
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-2 control-label" for="inputSenha2">Repita a senha</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="password" id="inputSenha2" value="" name="inputSenha2" onblur="verificaSenha()" placeholder="Repitir senha">
                        <span id="erro-senha" class="text-danger"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Editar</button>
                    </div>
                </div>
            </form>
            <br />

        </div>
    </div>
<?php }else{ ?>
    <div class="alert alert-danger text-center" role="alert"><strong>Usuário não existente</strong></div>
<?php }?>
<center><a class="btn btn-default" href="javascript:history.back()"> Voltar</a></center> <?php }} ?>