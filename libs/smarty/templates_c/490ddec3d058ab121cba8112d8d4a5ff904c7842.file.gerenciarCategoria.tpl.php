<?php /* Smarty version Smarty-3.1.13, created on 2016-01-28 01:01:15
         compiled from "/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gerenciarCategoria.tpl" */ ?>
<?php /*%%SmartyHeaderCode:66117336456a9847b5af1f4-70694574%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '490ddec3d058ab121cba8112d8d4a5ff904c7842' => 
    array (
      0 => '/home/www/html/siteMarko/admin/libs/smarty/templates/paginas/gerenciarCategoria.tpl',
      1 => 1452963810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '66117336456a9847b5af1f4-70694574',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'erroCategoria' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_56a9847b5bb664_02551833',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56a9847b5bb664_02551833')) {function content_56a9847b5bb664_02551833($_smarty_tpl) {?><h1 class="page-header">Cadastrar Categoria</h1>

<div class="row placeholders">
<?php echo $_smarty_tpl->tpl_vars['erroCategoria']->value;?>

    <form action="includes/controllers/cadastrarCategoria.php" method="post" id="formCadastrarCategoria" name="formCadastrarCategoria" class="form-horizontal" role="form">

        <div class="form-group form-group-sm">
            <label class="col-sm-2 control-label" for="inputCategoria">Nova Categoria</label>
            <div class="col-sm-4">
                <input class="form-control" type="text" id="inputCategoria" name="inputCategoria" required="" value="" placeholder="EX: Informática">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-1">
                <button type="submit" class="btn btn-success">Cadastrar</button>
            </div>
        </div>

    </form>

</div>

<?php }} ?>