<h1 class="page-header">Cadastrar Imagem do Produto {$idProdutoNovo}</h1>

<div class="row placeholders">
    {*
    <div class="img-prod">

    <h3>Selecione as imagens para o produto cadastrado</h3>
    <small>Selecione todas as imagens que irá aparecer para o produto pré-cadastrado no sistema.</small><br><br>
    <div id="container">
    <a id="pickfiles" class="btn btn-default btn-lg" href="javascript:;">Selecionar Imagens</a><br><br>
    <a id="uploadfiles" class="btn btn-primary" href="javascript:;">Enviar Imagens</a>
    </div>
    <br>
    <div id="filelist">Seu navegador não suporta HTML5 ou Flash.</div>
    <br>
    <!-- Progress Bar -->
    <div class="progressbar"></div>
    </div>
    <br>
    <pre id="console" class="alert-info"></pre>
    *}

    <div id="uploader">
        <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
    </div>

    {literal}

        <script type="text/javascript">
            // Initialize the widget when the DOM is ready
            $(function () {
                // Setup html5 version
                $("#uploader").pluploadQueue({
                    // General settings
                    runtimes: 'html5,flash,silverlight,html4',
                    url: "libs/plupload/examples/upload.php",
                    chunk_size: '1mb',
                    rename: true,
                    dragdrop: true,
                    filters: {
                        // Maximum file size
                        max_file_size: '10mb',
                        // Specify what files to browse for
                        mime_types: [
                            {title: "Image files", extensions: "jpg,gif,png"},
                            {title: "Zip files", extensions: "zip"}
                        ]
                    },
                    // Resize images on clientside if we can
                    resize: {
                        width: 700,
                        height: 500,
                        quality: 100,
                        crop: false // crop to exact dimensions
                    },
                    // Flash settings
                    flash_swf_url: '/plupload/js/Moxie.swf',
                    // Silverlight settings
                    silverlight_xap_url: '/plupload/js/Moxie.xap'
                });
            });
        </script>
    {/literal}
    <center><a href="produtosCadastrados.php?s=u" class="btn btn-primary btn-lg">Após o envio das imagens, clique aqui para sair</a></center>
</div>