<h2 class="sub-header">Categorias cadastradas <a href="gerenciarCategoria.php" type="button" class="btn btn-primary btn-xs"> Novo</a></h2> 
<div class="table-responsive">
    {$erroCategoria}
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome Categoria</th>
                <th>Data Cadastro</th>
                <th>Status</th>
                <th>Opções</th>
            </tr>
        </thead>
        <tbody>
            {foreach $categoria as $c}
                <tr>
                    <td>{$c->id_categoria_produto}</td>
                    <td>{$c->categoria_nome}</td>
                    <td>{$c->data_cadastro|date_format: "%d/%m/%Y"}</td>
                    <td>{if $c->status_categoria == "A"} <span class="text-success"><strong>Ativo</strong></span> {else} Desativado {/if}</td>
                    <td width="150">
                        <a href="editarCategoria.php?idEd={$c->id_categoria_produto}" class="btn btn-warning btn-xs">Editar</a>
                        <a href="{$c->id_categoria_produto}" class="btn btn-danger btn-xs">Desativar</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>