<div class="panel panel-default">

    <div class="panel-heading">

        <h2 class="panel-title">Cadastrar novo usuário</h2>
    </div>
    <div class="panel-body">

        {$erroUser} 
        <form action="includes/controllers/novoUsuario.php" method="post" name="cadastrar" class="form-horizontal" role="form" onSubmit="return verificaSenha()">
            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputNome">Nome Completo</label>
                <div class="col-sm-6">
                    <input class="form-control" type="text" id="inputNome" name="inputNome" required="" placeholder="Nome do Usuário">
                </div>
            </div>            

            <div class="row form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputLogin">Login</label>
                <div class="col-sm-3">
                    <input class="form-control" type="text" id="inputLogin" name="inputLogin" required="" placeholder="Login do usuário">

                </div>
                <div class="col-sm-3">
                    <select class="form-control" id="selectNivel" name="selectNivel" required="">
                        <option value="">Nivel do usuário</option>
                        <option value="0">Administrador</option>
                        <option value="1">Supervisor</option>
                        <option value="2">Usuário Básico</option>
                    </select>
                </div>
            </div>
            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputSenha">Senha</label>
                <div class="col-sm-3">
                    <input class="form-control" type="password" id="inputSenha" value="" name="inputSenha" required="" placeholder="Senha">
                </div>
            </div>
            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputSenha2">Repita a senha</label>
                <div class="col-sm-3">
                    <input class="form-control" type="password" id="inputSenha2" value="" name="inputSenha2" required="" onblur="verificaSenha()" placeholder="Repitir senha">
                    <span id="erro-senha" class="text-danger"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">Cadastrar</button>
                </div>
            </div>
        </form>
        <br />

    </div>
</div>

<h2 class="sub-header">Usuários Cadastrados</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Login</th>
                <th>Nível</th> 
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            {foreach $user as $u}
                <tr>
                    <td>{$u->id_usuario}</td>
                    <td width="400">{$u->nome_usuario}</td>
                    <td>{$u->login_usuario}</td>
                    <td>
                        {if $u->nivel_usuario == 0} 
                            Administrador 
                        {else if $u->nivel_usuario == 1}
                            Supervisor
                        {else if $u->nivel_usuario == 2}
                            Usuário Comum
                        {/if}
                    </td>
                    <td>
                        <a href="gerenciarUser.php?idUser={$u->id_usuario}" class="btn btn-warning btn-xs">Editar</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <center><a class="btn btn-default" href="javascript:history.back()"> Voltar</a></center> 