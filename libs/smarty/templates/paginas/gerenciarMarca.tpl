<h1 class="page-header">Cadastrar Marca</h1>

<div class="row placeholders">
{$erroMarca}
    <form action="includes/controllers/cadastrarMarca.php" method="post" id="formCadastrarCategoria" name="formCadastrarCategoria" class="form-horizontal" role="form">

        <div class="form-group form-group-sm">
            <label class="col-sm-2 control-label" for="inputMarca">Nova Marca</label>
            <div class="col-sm-4">
                <input class="form-control" type="text" id="inputMarca" name="inputMarca" required="" value="" placeholder="EX: Dell">
            </div>
        </div>
        
        <div class="form-group">
            <label for="textAreaDescMarca" class="col-sm-2 control-label">Descrição</label>
            <div class="col-sm-6 text-left" >
                <textarea class="form-control" id="textAreaDescMarca" name="textAreaDescMarca" rows="3"></textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-1">
                <button type="submit" class="btn btn-success">Cadastrar</button>
            </div>
        </div>

    </form>

</div>

