{if !empty($edUser)}

    <div class="panel panel-default">

        <div class="panel-heading">

            <h2 class="panel-title">Editar usuário</h2>
        </div>
        <div class="panel-body">

            {$erroUser} 
            <form action="includes/controllers/editarUsuario.php" method="post" name="cadastrar" class="form-horizontal" role="form" onSubmit="return verificaSenha()">
                <div class="form-group form-group-sm">
                    <label class="col-sm-2 control-label" for="inputNome">Nome Completo</label>
                    <div class="col-sm-6">
                        <input class="form-control" type="text" id="inputNome" name="inputNome" value="{$edUser->nome_usuario}" required="" placeholder="Nome do Usuário">
                    </div>
                    <input type="hidden" id="idUser" name="idUser" value="{$edUser->id_usuario}">
                </div>            

                <div class="row form-group form-group-sm">
                    <label class="col-sm-2 control-label" for="inputLogin">Login</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" id="inputLogin" name="inputLogin" value="{$edUser->login_usuario}" required="" placeholder="Login do usuário">

                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" id="selectNivel" name="selectNivel" required="">
                            <option value="{$edUser->nivel_usuario}">
                                {if $edUser->nivel_usuario == 0} 
                                    Administrador 
                                {else if $edUser->nivel_usuario == 1}
                                    Supervisor
                                {else if $edUser->nivel_usuario == 2}
                                    Usuário Comum
                                {/if}
                            </option>
                            <option value="0">Administrador</option>
                            <option value="1">Supervisor</option>
                            <option value="2">Usuário Básico</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-2 control-label" for="inputSenha">Senha</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="password" id="inputSenha" value="" name="inputSenha" placeholder="Senha">
                    </div>
                </div>
                <div class="form-group form-group-sm">
                    <label class="col-sm-2 control-label" for="inputSenha2">Repita a senha</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="password" id="inputSenha2" value="" name="inputSenha2" onblur="verificaSenha()" placeholder="Repitir senha">
                        <span id="erro-senha" class="text-danger"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Editar</button>
                    </div>
                </div>
            </form>
            <br />

        </div>
    </div>
{else}
    <div class="alert alert-danger text-center" role="alert"><strong>Usuário não existente</strong></div>
{/if}
<center><a class="btn btn-default" href="javascript:history.back()"> Voltar</a></center> 