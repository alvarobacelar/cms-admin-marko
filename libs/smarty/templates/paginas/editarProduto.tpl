<h1 class="page-header">Editar Produto</h1>

<div class="row placeholders">

    {if !empty($prodEdit)}

        <form action="includes/controllers/editarProduto.php" method="post" id="formEditarProduto" name="formEditarProduto" class="form-horizontal" role="form">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="selectGrupo">Grupo</label>
                <div class="col-sm-3">
                    <select class="form-control" id="selectGrupo" name="selectGrupo" required="">
                        <option value="{$grupo}">{$prodEdit->nome_grupo}</option>
                        {foreach $produto as $p}
                            <option value="{$p->id_grupo_produto}">{$p->nome_grupo}</option>
                        {/foreach}
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="selectMarca">Marca</label>
                <div class="col-sm-3">
                    <select class="form-control" id="selectMarca" name="selectMarca" required="">
                        <option value="{$prodEdit->id_marca_produto}">{$prodEdit->nome_marca}</option>
                        {foreach $marca as $m}
                            <option value="{$m->id_marca_produto}">{$m->nome_marca}</option>
                        {/foreach}
                    </select>
                </div>
                <input type="hidden" name="inputIdProduto" id="inputIdProduto" value="{$prodEdit->id_produto}">
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputProduto">Nome produto</label>
                <div class="col-sm-8">
                    <input class="form-control" type="text" id="inputProduto" name="inputProduto" required="" value="{$prodEdit->nome_produto}" placeholder="EX: Impressora HP ...">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputCod">Código</label>
                <div class="col-sm-3">
                    <input class="form-control" type="text" id="inputCod" name="inputCod" required="" value="{$prodEdit->cod_produto}" placeholder="EX: 1000999">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputPreco">Preço</label>
                <div class="col-sm-2">
                    <input class="form-control" type="text" id="inputPreco" name="inputPreco" required="" value="{$prodEdit->preco_produto}" placeholder="EX: 400,00">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputPrecoDesconto">Preço Desconto</label>
                <div class="col-sm-2">
                    <input class="form-control" type="text" id="inputPrecoDesconto" name="inputPrecoDesconto" value="{$prodEdit->preco_desconto_produto}" placeholder="EX: 320,00">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputPorcentDesconto">Porcentagem do Desconto</label>
                <div class="col-sm-2">
                    <input class="form-control" type="text" id="inputPorcentDesconto" name="inputPorcentDesconto" value="{$prodEdit->porcent_desconto_produto}" placeholder="EX: -30%">
                </div>
            </div>

            <div class="form-group">
                <label for="textAreaDescricao" class="col-sm-2 control-label">Descrição</label>
                <div class="col-sm-8 text-left" >
                    <textarea class="form-control" id="textAreaDescricao" name="textAreaDescricao" rows="3">{$prodEdit->descricao_produto}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="textAreaVisGeral" class="col-sm-2 control-label">Visão geral</label>
                <div class="col-sm-8 text-left">
                    <textarea class="form-control" id="textAreaVisGeral" name="textAreaVisGeral" rows="3">{$prodEdit->visao_geral_produto}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="textAreaInfoAdicinal" class="col-sm-2 control-label">Informação adicional</label>
                <div class="col-sm-8 text-left">
                    <textarea class="form-control" id="textAreaInfoAdicinal" name="textAreaInfoAdicinal" rows="3">{$prodEdit->info_adicional_produto}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputLinkVideo">Link Vídeo</label>
                <div class="col-sm-5">
                    <input class="form-control" type="text" id="inputLinkVideo" name="inputLinkVideo" value="{$prodEdit->link_video_produto}" placeholder="EX: https://www.youtube.com/watch?v=LVBW5IimwCU">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputLinkVideo">Opções</label>
                <div class="col-sm-offset-0 col-sm-6">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="chekDestaque" id="chekDestaque" {if $prodEdit->destaque_produto == "S"}checked=""{/if} value="{$prodEdit->destaque_produto}"> Destaque
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="chekEspeciais" id="chekEspeciais" {if $prodEdit->especial_produto == "S"}checked=""{/if} value="{$prodEdit->especial_produto}"> Especial
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="chekMaisVendidos" id="chekMaisVendidos" {if $prodEdit->mais_vendido_produto == "S"}checked=""{/if} value="{$prodEdit->mais_vendido_produto}"> Mais vendido
                    </label>
                    <label class="checkbox-inline">
                        <input type="checkbox" name="chekNovo" id="chekNovo" {if $prodEdit->novo_produto == "S"}checked=""{/if} value="S"> Etiqueta Novo
                    </label>
                </div>
            </div>
            <br>
            <div class = "form-group">
                <div class = "col-sm-offset-2 col-sm-1">
                    <button type = "submit" class = "btn btn-success"> Editar </button>
                </div>
            </div>

        </form>
    {else}
        <div class="alert alert-danger text-center" role="alert"><strong>Produto não existente</strong></div>
    {/if}

</div>

