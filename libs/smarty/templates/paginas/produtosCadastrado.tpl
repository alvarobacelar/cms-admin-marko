<h2 class="sub-header">
    {if $busca == ""}
        Produtos cadastrados <a href="gerenciarProduto.php" type="button" class="btn btn-primary btn-xs"> Novo</a>
    {else}
        Busca por: {$busca}
    {/if}
</h2>
<div class="table-responsive">
    {if !empty($cod_produto)}
        {$erroProduto}
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Cod</th>
                    <th>Nome Produto</th>
                    <th>Preço</th>
                    <th>Grupo Produto</th>
                    <th>Marca Produto</th>
                    <th>Data cadastro</th>
                    <th>Imagens</th>
                    <th>Status</th>
                    <th><center>Ações</center></th>
            </tr>
            </thead>
            <tbody>
                {section loop=$cod_produto name=p}
                    <tr>
                        <td>{$cod_produto[p]}</td>
                        <td>{$nome_produto[p]}</td>
                        <td>{$preco_produto[p]}</td>
                        <td>{$nome_grupo[p]}</td>
                        <td>{$nome_marca[p]}</td>
                        <td>{$data_cadastro_produto[p]|date_format: "%d/%m/%Y"}</td>
                        <td class="text-center" width="150">
                            {if $imagemProduto[p] == 0} 
                                <a href="gerenciarProduto.php?id={$id_produto[p]}" class="btn btn-info btn-xs">Cadastrar</a> 
                            {else} 
                                <span class="badge">
                                    {$imagemProduto[p]} imagens
                                </span><br>
                                <a href="gerenciarProduto.php?id={$id_produto[p]}" class="btn btn-warning btn-xs">Adicionar</a> 
                                <button onclick="excluirImagem({$id_produto[p]})" class="btn btn-danger btn-xs">Excluir</button>
                            {/if}
                        </td>
                        <td>
                            {if $disponibilidade_produto[p] == "S"} 
                                <span class="text-success">
                                    <i><strong>Disponível</strong></i>
                                </span> 
                                <button onclick="dispProduto({$id_produto[p]},'N')" class="btn btn-danger btn-xs">Tornar Indisponível</button>
                            {else} 
                                <span class="text-danger">
                                    <strong>Indisponível</strong>
                                </span>
                                <button onclick="dispProduto({$id_produto[p]},'S')" class="btn btn-success btn-xs">Tornar Disponível</button>
                            {/if}
                        </td>
                        <td>
                            
                            <a href="editarProduto.php?editar={$id_produto[p]}" title="Ao clicar irá mostrar no site" class="btn btn-info btn-xs">Editar</a>
                        </td>
                    </tr>
                {/section}
            </tbody>
        </table>
        {if !empty($paginacao)}
            {$paginacao}
        {/if}
    {else}
        <div class="alert alert-danger text-center" role="alert"><strong>Nada foi encontrado, tente buscar novamente.</strong></div>
    {/if}
</div>