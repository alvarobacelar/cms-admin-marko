<h1 class="page-header">Editar Categoria</h1>

<div class="row placeholders">

    {if !empty($catEdit)}
        <form action="includes/controllers/editarCategoria.php" method="post" id="formCadastrarCategoria" name="formCadastrarCategoria" class="form-horizontal" role="form">

            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputCategoria">Categoria</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" id="inputCategoria" name="inputCategoria" required="" value="{$catEdit->categoria_nome}" placeholder="EX: Informática">
                </div>
                <input type="hidden" id="idEd" name="idEd" value="{$catEdit->id_categoria_produto}" >
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-1">
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </div>

        </form>
    {else}
        <div class="alert alert-danger text-center" role="alert"><strong>Categoria não existente</strong></div>
    {/if}

</div>

