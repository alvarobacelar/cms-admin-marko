<h2 class="sub-header">Grupos cadastrados <a href="gerenciarGrupo.php" type="button" class="btn btn-primary btn-xs"> Novo</a></h2>
<div class="table-responsive">
    {$erroGrupo}
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome grupo</th>
                <th>Categoria do grupo</th>
                <th>Data cadastro</th>
                <th>Status</th>
                <th><center>Ações</center></th>
            </tr>
        </thead>
        <tbody>
            {foreach $produto as $p}
                <tr>
                    <td>{$p->id_grupo_produto}</td>
                    <td>{$p->nome_grupo}</td>
                    <td>{$p->categoria_nome}</td>
                    <td>{$p->data_cadastro_grupo|date_format: "%d/%m/%Y"}</td>
                    <td>{if $p->status_grupo == "A"} <span class="text-success"><strong>Ativo</strong></span> {else} <span class="text-danger"><strong>Desativado</strong></span> {/if}</td>
                    <td width="150">
                        <a href="editarGrupo.php?idEdit={$p->id_grupo_produto}" class="btn btn-warning btn-xs">Editar</a>
                        <a class="btn btn-danger btn-xs">Desativar</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>