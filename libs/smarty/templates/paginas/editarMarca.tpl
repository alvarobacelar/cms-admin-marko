<h1 class="page-header">Editar Marca</h1>

<div class="row placeholders">
    {if !empty($marcaEdit)}
        {$erroMarca}
        <form action="includes/controllers/editarMarca.php" method="post" id="formCadastrarCategoria" name="formCadastrarCategoria" class="form-horizontal" role="form">

            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputMarca">Marca</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" id="inputMarca" name="inputMarca" required="" value="{$marcaEdit->nome_marca}" placeholder="EX: Dell">
                </div>
                <input type="hidden" id="idEdMarca" name="idEdMarca" value="{$marcaEdit->id_marca_produto}">
            </div>

            <div class="form-group">
                <label for="textAreaDescMarca" class="col-sm-2 control-label">Descrição</label>
                <div class="col-sm-6 text-left" >
                    <textarea class="form-control" id="textAreaDescMarca" name="textAreaDescMarca" rows="3">{$marcaEdit->descricao_marca}</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-1">
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </div>

        </form>
    {else}
        <div class="alert alert-danger text-center" role="alert"><strong>Marca não existente</strong></div>
    {/if}

</div>

