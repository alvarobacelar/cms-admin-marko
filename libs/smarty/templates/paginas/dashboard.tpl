<h1 class="page-header">Painel Inicial</h1>

<div class="row placeholders">
    <div class="col-xs-6 col-sm-3 placeholder">
        <a href="./gerenciarProduto.php">
            <img src="imagens/prod.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
            <h4>Produto</h4>
            <span class="text-muted">Novo Produto</span>
        </a>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <a href="./gerenciarCategoria.php">
            <img src="imagens/categ.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
            <h4>Categoria</h4>
            <span class="text-muted">Nova Categoria</span>
        </a>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <a href="./gerenciarGrupo.php">
            <img src="imagens/grupo.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
            <h4>Grupo</h4>
            <span class="text-muted">Novo Grupo</span>
        </a>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <a href="./gerenciarMarca.php">
            <img src="imagens/marca.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
            <h4>Marca</h4>
            <span class="text-muted">Nova Marca</span>
        </a>
    </div>
</div>

<h2 class="sub-header">Recentes</h2>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Produto</th>
                <th>Preço</th>
                <th>Grupo</th>
                <th>Marca</th>
                <th>Data Cadastro</th>
            </tr>
        </thead>
        <tbody>
            {foreach $prodHome as $ph}
                <tr>
                    <td>{$ph->cod_produto}</td>
                    <td width="400">{$ph->nome_produto}</td>
                    <td>{$ph->preco_produto}</td>
                    <td>{$ph->nome_grupo}</td>
                    <td>{$ph->nome_marca}</td>
                    <td>{$ph->data_cadastro_produto|date_format: "%d/%m/%Y"}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
    <center><a href="produtosCadastrados.php" class="btn btn-default">Veja Mais</a></center>
</div>