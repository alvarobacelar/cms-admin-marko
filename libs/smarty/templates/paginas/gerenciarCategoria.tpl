<h1 class="page-header">Cadastrar Categoria</h1>

<div class="row placeholders">
{$erroCategoria}
    <form action="includes/controllers/cadastrarCategoria.php" method="post" id="formCadastrarCategoria" name="formCadastrarCategoria" class="form-horizontal" role="form">

        <div class="form-group form-group-sm">
            <label class="col-sm-2 control-label" for="inputCategoria">Nova Categoria</label>
            <div class="col-sm-4">
                <input class="form-control" type="text" id="inputCategoria" name="inputCategoria" required="" value="" placeholder="EX: Informática">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-1">
                <button type="submit" class="btn btn-success">Cadastrar</button>
            </div>
        </div>

    </form>

</div>

