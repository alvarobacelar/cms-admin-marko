<h1 class="page-header">Editar Grupo</h1>

<div class="row placeholders">

    {if !empty($dbEdit)}

        {$erroGrupo}
        <form action="includes/controllers/editarGrupo.php" enctype="multipart/form-data" method="post" id="formCadastrarGrupo" name="formCadastrarGrupo" class="form-horizontal" role="form">

            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="inputGrupo">Nome Grupo</label>
                <div class="col-sm-4">
                    <input class="form-control" type="text" id="inputGrupo" name="inputGrupo" required="" value="{$dbEdit->nome_grupo}" placeholder="EX: Notebooks">
                </div>
                <input type="hidden" name="idGrupo" id="idGrupo" value="{$dbEdit->id_grupo_produto}" >
            </div>

            <div class="form-group form-group-sm">
                <label class="col-sm-2 control-label" for="selectCategoria">Categoria</label>
                <div class="col-sm-3">
                    <select class="form-control" id="selectNivel" name="selectCategoria" required="">
                        <option value="{$dbEdit->id_categoria_produto}">{$dbEdit->categoria_nome}</option>
                        {foreach $categoria as $c}
                            <option value="{$c->id_categoria_produto}">{$c->categoria_nome}</option>
                        {/foreach}
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="textAreaDescricaoGp" class="col-sm-2 control-label">Descrição Grupo</label>
                <div class="col-sm-8 text-left" >
                    <textarea class="form-control" id="textAreaDescricaoGp" name="textAreaDescricaoGp">{$dbEdit->descricao_grupo}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="fileGrupo">Imagem do Grupo</label>
                <div class="col-sm-6">
                    <input class="form-control" type="file" id="fileGrupo" value="" name="fileGrupo" ><br />
                </div>
            </div>  

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-1">
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </div>

        </form>
    {else}
        <div class="alert alert-danger text-center" role="alert"><strong>Grupo não existente</strong></div>
    {/if}

</div>
