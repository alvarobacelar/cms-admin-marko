<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <link rel="icon" href="../../favicon.ico">

        <title>{$titulo}</title>

        <!-- Bootstrap core CSS -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
        <link type="text/css" rel="stylesheet" href="css/shCore.css" media="screen" />
        <link type="text/css" rel="stylesheet" href="css/jquery.ui.plupload.css" media="screen" />
        <link type="text/css" rel="stylesheet" href="libs/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css" media="screen" />

        <!-- Custom styles for this template -->
        <link type="text/css" rel="stylesheet" href="css/dashboard.css" >

        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery.min.js"></script>        
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/shCore.js" charset="UTF-8"></script>
        <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
        <script type="text/javascript" src="js/ie-emulation-modes-warning.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script type="text/javascript" src="js/ie10-viewport-bug-workaround.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js" charset="UTF-8"></script>
        <script type="text/javascript" src="libs/plupload/js/plupload.full.min.js"></script>
        <script type="text/javascript" src="libs/plupload/js/jquery.plupload.queue/jquery.plupload.queue.min.js"></script>
        <script type="text/javascript" src="libs/plupload/js/i18n/pt_BR.js"></script>
        <script type="text/javascript" src="js/themeswitcher.js" charset="UTF-8"></script>
        <script type="text/javascript" src="js/valida.js"></script>
        <!-- debug 
            <script type="text/javascript" src="../js/moxie.js"></script>
            <script type="text/javascript" src="../js/plupload.dev.js"></script>
        -->
    </head>

    <body>

        {literal}
            <script type="text/javascript" src="libs/nicEdit/nicEdit.js"></script> 
            <script type="text/javascript">
                //<![CDATA[
                bkLib.onDomLoaded(function () {
                    nicEditors.allTextAreas()
                });
                //]]>
            </script>
        {/literal}

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">CMS Marko Informática</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a>Bem vindo, {$nomeUser}</a></li>
                        <li><a href="../" target="_black"><strong>Ver Site</strong></a></li>
                            {*<li><a href="#">Profile</a></li>*}
                        <li><a href="./logOUT.php">Sair</a></li>
                    </ul>
                    <form method="get" action="produtosCadastrados.php" class="navbar-form navbar-right">
                        <input type="text" class="form-control" name="bp" id="bp" placeholder="Buscar produtos...">
                    </form>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 col-md-2 sidebar">
                    <ul class="nav nav-sidebar">
                        <li class="active"><a href="./"><strong>Painel Inicial</strong> <span class="sr-only">(current)</span></a></li>
                        <li><a href="produtosCadastrados.php">Produtos Cadastrados</a></li>
                        <li><a href="categoriasCadastradas.php">Categorias Cadastradas</a></li>
                        <li><a href="gruposCadastrados.php">Grupos Cadastrados</a></li>
                        <li><a href="marcasCadastradas.php">Marcas Cadastradas</a></li>
                    </ul>
                    <ul class="nav nav-sidebar">    
                        <li><a href="gerenciarProduto.php">Novo Produto</a></li>
                        <li><a href="gerenciarCategoria.php">Nova Categoria</a></li>
                        <li><a href="gerenciarGrupo.php">Novo Grupo</a></li>
                        <li><a href="gerenciarMarca.php">Nova Marca</a></li>
                    </ul>
                    <ul class="nav nav-sidebar">
                        <li><a href="gerenciarUser.php">Gerenciar Usuário</a></li>
                        <li><a href="logUser.php">Log de Usuário</a></li>
                    </ul>
                    <ul class="nav nav-sidebar cred">
                        <li class="text-center">
                            © 2016 CMS Marko Informática<br> 
                            Desenvolvido por 
                            <a href="http://www.alvarobacelar.com" target="_blank">Álvaro Bacelar</a>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                    <ol class="breadcrumb">
                        {$local}
                    </ol>

                    {include file=$conteudo}

                </div>
            </div>
        </div>

    </body>
</html>
