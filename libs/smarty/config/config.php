<?php


// arquivos de configuração inicial do Smarty config.php
// @author Álvaro Bacelar
// @date 14/01/2016
// hack para rodar em linux e windows
// Smarty is assumend to be in 'includes/' dir under current script
define('UPLOAD_DIR',str_replace("\\", "/", getcwd()) . '/upload');
define('SMARTY_DIR', str_replace("\\", "/", getcwd()) . '/libs/smarty/lib/');
define("TEMPLATE", "");
require_once(SMARTY_DIR . 'Smarty.class.php');
$smarty = new Smarty();
$smarty->template_dir = str_replace("\\", "/", getcwd()) . "/libs/smarty/templates/" . TEMPLATE;
$smarty->compile_dir = str_replace("\\", "/", getcwd()) . "/libs/smarty/templates_c/";
$smarty->config_dir = str_replace("\\", "/", getcwd()) . "/libs/smarty/setup/";
$smarty->cache_dir = str_replace("\\", "/", getcwd()) . "/libs/smarty/cache/";

//$smarty->debugging = true; // debug para possivel verificação


