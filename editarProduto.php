<?php

require_once './libs/smarty/config/config.php';
require_once './includes/funcoes/verifica.php';
require_once './includes/models/ManipulateData.php';

if ($estaLogado == "SIM") {
    if (isset($_GET["editar"])) {

        $idProdutoEdit = addslashes($_GET["editar"]);


        /*
         * Realizando a busca no banco de dados para editar produto
         */
        $editProduto = new ManipulateData();
        $editProduto->setOrderTable("AND produto.id_produto = '$idProdutoEdit'");
        $editProduto->selectProdutos();
        $prodEditar = $editProduto->fetch_object();
        if (!empty($prodEditar)) {
            $smarty->assign("grupo", $prodEditar->id_grupo_produto);
            $smarty->assign("prodEdit", $prodEditar);
            // fim da busca
        }


        /*
         * Realizando pesquisa no banco de dados de grupos de produtos cadastrados
         */
        $produto = new ManipulateData();
        $produto->setTable("grupo_produto");
        $produto->setOrderTable("ORDER BY nome_grupo");
        $produto->select();
        while ($db[] = $produto->fetch_object()) {
            $smarty->assign("produto", $db);
        } // FIM DA PESQUISA DE GRUPOS DE PRODUTOS

        /*
         * Realizadno pesquisa do banco de dados das marcas
         */
        $marca = new ManipulateData();
        $marca->setTable("marca_produto");
        $marca->setOrderTable("ORDER BY nome_marca");
        $marca->select();
        while ($dbMarca[] = $marca->fetch_object()) {
            $smarty->assign("marca", $dbMarca);
        }

        $local = "<li><a href='./'>Painel Incial</a></li>
            <li><a href='./produtosCadastrados.php'>Produtos Cadastrados</a></li>
            <li><a href='#'>Editar Produto</a></li>";
        $smarty->assign("local", $local);
        $smarty->assign("titulo", "Editar Produto - Marko");
        $smarty->assign("conteudo", "paginas/editarProduto.tpl");
        $smarty->display("layout.tpl");
    }
}