<?php

require_once './libs/smarty/config/config.php';
require_once './includes/models/ManipulateData.php';
require_once './includes/funcoes/verifica.php';

if ($estaLogado == "SIM") {

    /*
     * Realizando pesquisa no banco de dados de grupos de produtos cadastrados
     */
    $produto = new ManipulateData();
    $produto->setTable("grupo_produto");
    $produto->setOrderTable("ORDER BY nome_grupo");
    $produto->select();
    while ($db[] = $produto->fetch_object()) {
        $smarty->assign("produto", $db);
    } // FIM DA PESQUISA DE GRUPOS DE PRODUTOS

    /*
     * Realizadno pesquisa do banco de dados das marcas
     */
    $marca = new ManipulateData();
    $marca->setTable("marca_produto");
    $marca->setOrderTable("ORDER BY nome_marca");
    $marca->select();
    while ($dbMarca[] = $marca->fetch_object()) {
        $smarty->assign("marca", $dbMarca);
    }


    /*
     * Caso o usuário venha da página de produtos cadastrados, 
     * para cadastrar uma nova imagem ou mudar a imagem, será criado as sessions 
     * necessárias para ir para a página de cadastro de imagem
     */
    if (isset($_GET["id"])) {
        $_SESSION["idProdutoNovo"] = addslashes($_GET["id"]);
        $_SESSION["erroProduto"] = "OK";
    }

    /*
     * Verificando o retorno do cadastro no banco de dados com a session "erroProduto"
     */
    if (isset($_SESSION["erroProduto"])) {
        $erro = $_SESSION["erroProduto"];
        if ($erro == "duplicado") {
            header("location: produtosCadastrados.php");
        } else
        if ($erro == "OK") {
            $idProdutoNovo = $_SESSION["idProdutoNovo"]; // recebendo o id do produto novo cadastrado recente
            $smarty->assign("idProdutoNovo", $idProdutoNovo); // passando o id do produto cadastrado

            /*
             * Parâmetros básicos para a pagina de cadastro de imagens
             */
            $local = "<li><a href='./'>Painel Incial</a></li>
                      <li class='active'>Novo Produto</li>
                      <li class='active'>Cadastrar Imagem do produto</li>";
            $smarty->assign("local", $local);
            $smarty->assign("titulo", "Imagem do Produto - Marko");
            $smarty->assign("conteudo", "paginas/gerenciarImagem.tpl");
            $smarty->display("layout.tpl");
        } else {
            $smarty->assign("erroProduto", "<div class='alert alert-danger' role='alert'>Erro! Se este erro persistir, procure o administrador do sistema</div>");
        }
    } else 
        if (!isset ($_SESSION["erroProduto"])) {
        $smarty->assign("erroProduto", "");

        /*
         *  Se, e somente se, o retorno da session for diferente de "OK" ele não 'destroi' a session
         *  pois com a session igual a "OK" o usuário vai cadastrar a imagem e somente após o cadastro
         *  da imagem é que ela será destruida. 
         */
        if (isset($_SESSION["erroProduto"])) {
            if ($_SESSION["erroProduto"] != "OK") {
                unset($_SESSION["erroProduto"]);
            }
        }

        if (isset($_SESSION["erroImagem"]) && $_SESSION["erroImagem"] == "img") {
            unset($_SESSION["erroImagem"]);
        }

        /*
         * Setando os parâmetros do smarty para a página produto
         */
        $local = "<li><a href='./'>Painel Incial</a></li>
                    <li class='active'>Novo Produto</li>";
        $smarty->assign("local", $local);
        $smarty->assign("titulo", "Gerenciar Produto - Marko");
        $smarty->assign("conteudo", "paginas/gerenciarProduto.tpl");
        $smarty->display("layout.tpl");
    } else {
        header("location: ./erro.php");
    }
}
        