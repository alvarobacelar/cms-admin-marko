<?php

require_once './libs/smarty/config/config.php';
require_once './includes/models/ManipulateData.php';
require_once './includes/funcoes/verifica.php';

if ($estaLogado == "SIM") {

    /*
     * Realizando a verificação de retorno de cadastro no banco de dados
     * caso retone uma session com "erroMarca" o sistema verifica qual mensagem mostrar para o usuário
     */
    if (isset($_SESSION["erroMarca"])) {
        $erro = $_SESSION["erroMarca"];
        if ($erro == "duplicado") {
            $smarty->assign("erroMarca", "<div class='alert alert-danger' role='alert'>Erro! Marca já cadastrada</div>");
        } else
        if ($erro == "OK") {
            $smarty->assign("erroMarca", "<div class='alert alert-success' role='alert'>Marca cadastrada com sucesso!</div>");
        } else
        if ($erro == "editado") {
            $smarty->assign("erroMarca", "<div class='alert alert-success' role='alert'>Marca editado com sucesso!</div>");
        } else {
        $smarty->assign("erroMarca", "<div class='alert alert-danger' role='alert'>Erro! ". $_SESSION["erroMarca"] ."</div>");
        }
    } else {
        $smarty->assign("erroMarca", "");
    }
    unset($_SESSION["erroMarca"]);

    /*
     * Buscando no banco de dados todas as marcas cadastradas
     */
    $marcaSec = new ManipulateData();
    $marcaSec->setTable("marca_produto");
    $marcaSec->setOrderTable("ORDER BY nome_marca");
    $marcaSec->select();
    while ($dbMarca[] = $marcaSec->fetch_object()) {
        $smarty->assign("marcas", $dbMarca);
    } // FIM DA BUSCA DAS MARCAS CADASTRADAS

    /*
     * Setando os parâmetros básicos do smarty para página marcas cadastradas
     */
    $local = "<li><a href='./'>Painel Incial</a></li>
        <li class='active'>Marcas Cadastradas</li>";
    $smarty->assign("local", $local);
    $smarty->assign("titulo", "Marcas Cadastradas - Marko");
    $smarty->assign("conteudo", "paginas/marcasCadastradas.tpl");
    $smarty->display("layout.tpl");
}