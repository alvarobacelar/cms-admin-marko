<?php

require_once './libs/smarty/config/config.php';
require_once './includes/models/ManipulateData.php';
require_once './includes/funcoes/verifica.php';

if ($estaLogado == "SIM") {

    if (isset($_GET["idEdMar"])) {
        
        $idMarcaEd = addslashes($_GET["idEdMar"]);

        /*
         * Realizando a verificação de retorno de cadastro no banco de dados
         * caso retone uma session com "erroCategoria" o sistema verifica qual mensagem mostrar para o usuário
         */
        if (isset($_SESSION["erroMarca"])) {
            $erro = $_SESSION["erroMarca"];
            if ($erro == "duplicado") {
                $smarty->assign("erroMarca", "<div class='alert alert-danger' role='alert'>Erro! Marca já cadastrada</div>");
            } else
            if ($erro == "OK") {
                $smarty->assign("erroMarca", "<div class='alert alert-success' role='alert'>Marca cadastrada com sucesso!</div>");
            } else {
                $smarty->assign("erroMarca", "<div class='alert alert-danger' role='alert'>Erro! Se este erro persistir, procure o administrador do sistema</div>");
            }
        } else {
            $smarty->assign("erroMarca", "");
        }
        unset($_SESSION["erroMarca"]);

        /*
         * Buscando no banco de dados a marca escolhida para edição
         */
        $editMarca = new ManipulateData();
        $editMarca->setTable("marca_produto");
        $editMarca->setFieldId("id_marca_produto");
        $editMarca->setValueId($idMarcaEd);
        $editMarca->selectAlterar();
        $dbMarEd = $editMarca->fetch_object();
        $smarty->assign("marcaEdit", $dbMarEd);

        /*
         * Setando os parâmetros do Smarty da página cadastrar Marca
         */
        $local = "<li><a href='./'>Painel Incial</a></li>
        <li><a href='./marcasCadastradas.php'>Marcas Cadastradas</a></li>
        <li class='active'>Nova Marca</li>";
        $smarty->assign("local", $local);
        $smarty->assign("titulo", "Gerenciar Marca - Marko");
        $smarty->assign("conteudo", "paginas/editarMarca.tpl");
        $smarty->display("layout.tpl");
    } else {
        header("location: ./erro.php");
    }
}