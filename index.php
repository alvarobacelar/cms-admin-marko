<?php

require_once './libs/smarty/config/config.php';
require_once './includes/funcoes/verifica.php';
require_once './includes/models/ManipulateData.php';

if ($estaLogado == "SIM") {

    $proCad = new ManipulateData();
    $proCad->setOrderTable("ORDER BY produto.id_produto DESC LIMIT 10");
    $proCad->selectProdutos();
    while ($dbProdHome[] = $proCad->fetch_object()){
        $smarty->assign("prodHome", $dbProdHome);
    }
    
    $local = "<li class='active'>Painel Inicial</li>";
    $smarty->assign("local", $local);
    $smarty->assign("titulo", "Pagina Inicial - Marko");
    $smarty->assign("conteudo", "paginas/dashboard.tpl");
    $smarty->display("layout.tpl");
}