<?php

require_once './libs/smarty/config/config.php';
require_once './includes/models/ManipulateData.php';
require_once './includes/funcoes/verifica.php';

if ($estaLogado == "SIM") {

    if (isset($_GET["idEd"])) {
        
        $idEditCat = addslashes($_GET["idEd"]);

        /*
         * Selecionando no banco de dados a categoria para editar
         */
        $categoriaEd = new ManipulateData();
        $categoriaEd->setTable("categoria_produto");
        $categoriaEd->setFieldId("id_categoria_produto");
        $categoriaEd->setValueId($idEditCat);
        $categoriaEd->selectAlterar();
        $dbCat = $categoriaEd->fetch_object();
        $smarty->assign("catEdit", $dbCat);

        /*
         * Setando os parâmetros do Smarty da página Categoria
         */
        $local = "<li><a href='./'>Painel Incial</a></li>
        <li><a href='./categoriasCadastradas.php'>Categorias Cadastradas</a></li>
        <li class='active'>Editar Categoria</li>";
        $smarty->assign("local", $local);
        $smarty->assign("titulo", "Editar Categoria - Marko");
        $smarty->assign("conteudo", "paginas/editarCategoria.tpl");
        $smarty->display("layout.tpl");
    } else {
        header("location: ./erro.php");
    }
}
