<?php

require_once './libs/smarty/config/config.php';
require_once './includes/models/ManipulateData.php';
require_once './includes/funcoes/verifica.php';

if ($estaLogado == "SIM") {

    if (isset($_GET["idEdit"])) {
        $idEdit = addslashes($_GET["idEdit"]);

        /*
         * Realizando a verificação de retorno de cadastro no banco de dados
         * caso retone uma session com "erroCategoria" o sistema verifica qual mensagem mostrar para o usuário
         */
        if (isset($_SESSION["erroGrupo"])) {
            $erro = $_SESSION["erroGrupo"];
            if ($erro == "duplicado") {
                $smarty->assign("erroGrupo", "<div class='alert alert-danger' role='alert'>Erro! Grupo já cadastrado</div>");
            } else
            if ($erro == "OK") {
                $smarty->assign("erroGrupo", "<div class='alert alert-success' role='alert'>Grupo cadastrado com sucesso!</div>");
            } else {
                $smarty->assign("erroGrupo", "<div class='alert alert-danger' role='alert'>Erro! " . $_SESSION["erroGrupo"] . "</div> ");
            }
        } else {
            $smarty->assign("erroGrupo", "");
        }
        unset($_SESSION["erroGrupo"]);
        
        /*
         * Buscando no banco de dados o grupo para edição
         */
        $editGrupo = new ManipulateData();
        $editGrupo->setTable("grupo_produto,categoria_produto");
        $editGrupo->setOrderTable("AND id_grupo_produto = '$idEdit'");
        $editGrupo->selectGrupoCat();
        $dbEdit = $editGrupo->fetch_object();
        $smarty->assign("dbEdit", $dbEdit);

        /*
         * Buscando no banco de dados todas as categorias cadastradas
         */
        $secCategoria = new ManipulateData();
        $secCategoria->setTable("categoria_produto");
        $secCategoria->setOrderTable("ORDER BY categoria_nome");
        $secCategoria->select();
        while ($dbGrupo[] = $secCategoria->fetch_object()) {
            $smarty->assign("categoria", $dbGrupo);
        } // FIM DA BUSCA DAS CATEGORIAS CADASTRADAS

        /*
         * Setando os parâmetros do smarty da página Grupo
         */
        $local = "<li><a href='./'>Painel Incial</a></li>
            <li><a href='./gruposCadastrados.php'>Grupos Cadastrados</a></li>
        <li class='active'>Editar Grupo</li>";
        $smarty->assign("local", $local);
        $smarty->assign("titulo", "Editar Grupo - Marko");
        $smarty->assign("conteudo", "paginas/editarGrupo.tpl");
        $smarty->display("layout.tpl");
    } else {
        header("location: ./erro.php");
    }
}
