<?php

require_once './libs/smarty/config/config.php';
require_once './includes/funcoes/verifica.php';
require_once './includes/models/ManipulateData.php';

if ($estaLogado == "SIM" && !isset($active)) {


    if ($_SESSION["nivel"] == "admin") { // se o usuário logado fo administrador, mostra a página
        // verificação de erro de senhas ou de duplicação de usuário
        if (isset($_SESSION["erroUser"])) {
            if ($_SESSION["erroUser"] == "erro") {
                $smarty->assign("erroUser", "<div class='alert alert-danger' role='alert'>ERRO! As senhas não conferem</div>");
            } else if ($_SESSION["erroUser"] == "duplicado") {
                $smarty->assign("erroUser", "<div class='alert alert-danger' role='alert'>ERRO! Usuário já cadastrado</div>");
            } else if ($_SESSION["erroUser"] == "OK") {
                $smarty->assign("erroUser", "<div class='alert alert-success' role='alert'>Usuário cadastrado com sucesso!</div>");
            } else if ($_SESSION["erroUser"] == "editado") {
                $smarty->assign("erroUser", "<div class='alert alert-success' role='alert'>Usuário editado com sucesso!</div>");
            } else {
                $smarty->assign("erroUser", "<div class='alert alert-danger' role='alert'>ERRO! " . $_SESSION["erroUser"] . " </div>");
            }
        } else {
            $smarty->assign("erroUser", "");
        }
        unset($_SESSION["erroUser"]);
        // fim da verificação de errro de cadastro

        if (!isset($_GET["idUser"])) {

            $user = new ManipulateData();
            $user->setTable("usuario");
            $user->setOrderTable("WHERE nome_usuario != 'OFF' ORDER BY nome_usuario");
            $user->select();
            while ($usr[] = $user->fetch_object()) {
                $smarty->assign("user", $usr);
            }

            $local = "<li><a href='./'>Painel Incial</a></li>
            <li class='active'>Novo Usuário</li>";
            $smarty->assign("local", $local);
            $smarty->assign("titulo", "Novo Usuário - Marko");
            $smarty->assign("conteudo", "paginas/cadastrarUser.tpl");
            $smarty->display("layout.tpl");
        } else {
            $idUsr = addslashes($_GET["idUser"]);

            /*
             * Buscando o usuário requisitado no banco de dados
             */
            $edUser = new ManipulateData();
            $edUser->setTable("usuario");
            $edUser->setFieldId("id_usuario");
            $edUser->setValueId($idUsr);
            $edUser->selectAlterar();
            $dbEdUser = $edUser->fetch_object();
            $smarty->assign("edUser", $dbEdUser);

            $local = "<li><a href='./'>Painel Incial</a></li>
            <li class='active'>Editar Usuário</li>";
            $smarty->assign("local", $local);
            $smarty->assign("titulo", "Editar Usuário - Marko");
            $smarty->assign("conteudo", "paginas/editarUser.tpl");
            $smarty->display("layout.tpl");
        }
    } else {

        header("location: ./accessDenied.php");
    }
}